import org.junit.Assert;
import org.junit.Test;

public class VectorCodeTest {
    private int[] arrayWithZero = {0, 1, 0, 6, 0};
    private int[] arrayWithoutZero = {9, 10, 15, 16, 20};
    private int[] arrayRepeatingElem = {1, 2, 1, 4, 1};

    @Test
    public void decodeWithZero() {
        VectorCode v = new VectorCode(arrayWithZero);
        Assert.assertTrue(equalsArr(v.decode(), arrayWithZero));
    }

    @Test
    public void decodeWithoutZero() {
        VectorCode v = new VectorCode(arrayWithoutZero);
        Assert.assertTrue(equalsArr(v.decode(), arrayWithoutZero));
    }

    @Test
    public void insertToOpenPosShouldBeAppearElem() {
        VectorCode v = new VectorCode(arrayWithZero);
        int[] ar = {0, 1, 3, 6, 0};
        v.insert(3, 2);
        Assert.assertTrue(v.equals(new VectorCode(ar)));
    }

    @Test
    public void insertToExistPosShouldBeReplaceElem() {
        VectorCode v = new VectorCode(arrayWithoutZero);
        int[] ar = {9, 5, 15, 16, 20};
        v.insert(5, 1);
        Assert.assertTrue(v.equals(new VectorCode(ar)));
    }

    @Test
    public void deleteExistElemShouldBeZero() {
        VectorCode v = new VectorCode(arrayWithoutZero);
        int[] ar = {9, 10, 15, 0, 20};
        v.delete(3);
        Assert.assertTrue(v.equals(new VectorCode(ar)));
    }

    @Test
    public void deleteZeroElemShouldBeNothing() {
        VectorCode v = new VectorCode(arrayWithZero);
        int[] ar = {0, 1, 0, 6, 0};
        v.delete(4);
        Assert.assertTrue(v.equals(new VectorCode(ar)));
    }

    @Test
    public void scalarProductWithZeroElem() {
        VectorCode v1 = new VectorCode(arrayWithZero);
        VectorCode v2 = new VectorCode(arrayWithoutZero);
        int sp = 0;
        for (int i = 0; i < Math.min(arrayWithoutZero.length, arrayWithZero.length); i++) {
            sp += arrayWithoutZero[i] * arrayWithZero[i];
        }
        Assert.assertEquals(v1.scalarProduct(v2), sp);
    }

    @Test
    public void scalarProductWithoutZeroElem() {
        VectorCode v1 = new VectorCode(arrayWithoutZero);
        VectorCode v2 = new VectorCode(arrayWithoutZero);
        int sp = 0;
        for (int i = 0; i < arrayWithoutZero.length; i++) {
            sp += arrayWithoutZero[i] * arrayWithoutZero[i];
        }
        Assert.assertEquals(v1.scalarProduct(v2), sp);
    }

    @Test
    public void sumShouldBeSum() {
        VectorCode v1 = new VectorCode(arrayWithoutZero);
        VectorCode v2 = new VectorCode(arrayWithZero);
        int[] a = {9, 11, 15, 22, 20};
        Assert.assertTrue(v1.sum(v2).equals(new VectorCode(a)));
    }

    @Test
    public void vectorSumWithZeroElem() {
        VectorCode v = new VectorCode(arrayWithZero);
        int[] a = {0, 0, 6, 6, 7, 7};
        Assert.assertTrue(v.vectorSum().equals(new VectorCode(a)));
    }

    @Test
    public void vectorSumWithoutZeroElem() {
        VectorCode v = new VectorCode(arrayWithoutZero);
        int[] a = {0, 20, 36, 51, 61, 70};
        Assert.assertTrue(v.vectorSum().equals(new VectorCode(a)));
    }

    @Test
    public void multWithRepeatingElem1ShouldBeX6() {
        VectorCode v = new VectorCode(arrayRepeatingElem);
        int[] a = {6, 2, 6, 4, 6};
        v.mult(1, 6);
        Assert.assertTrue(v.equals(new VectorCode(a)));
    }

    @Test
    public void multWithoutRepeatingElem1ShouldBeX6() {
        VectorCode v = new VectorCode(arrayWithoutZero);
        int[] a = {9, 70, 15, 16, 20};
        v.mult(10, 7);
        Assert.assertTrue(v.equals(new VectorCode(a)));
    }

    private static boolean equalsArr(int[] a, int[] b) {
        if (a.length == b.length) {
            for (int i = 0; i < a.length; i++) {
                if (a[i] != b[i])
                    return false;
            }
            return true;
        }
        return false;
    }
}
