import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int[] a = {1, 5, 0, 3, 2, 0, 66, 0, 0, 0};
        VectorCode vc = new VectorCode(a);
        VectorCode vc1 = new VectorCode(a);
        //decode
        vc.printVector();
        //insert
        vc.insert(1, 2);
        vc.printVector();
        //delete
        vc.delete(1);
        vc.printVector();
        //vectorSum
        VectorCode vsum = vc.vectorSum();
        vsum.printVector();
        //scalarProduct
        System.out.println(vsum.scalarProduct(vc));
        //mult
        vc.mult(1, 2);
        vc.printVector();
        //sum
        VectorCode vs = vc.sum(vc1);
        vs.printVector();
    }
}
