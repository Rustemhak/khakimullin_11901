import static java.lang.Math.*;

class VectorCode {
    private int length;
    private Elem head;

    VectorCode(int[] arr) {
        length = arr.length;
        for (int i = 0; i < length; i++) {
            if (arr[i] != 0) {
                Elem p = new Elem();
                p.value = arr[i];
                p.index = i;
                p.next = head;
                head = p;
            }
        }
    }

    int[] decode() {
        int i = length - 1;
        int[] a = new int[length];
        Elem p = head;
        while (p != null) {
            if (i == p.index) {
                a[i] = p.value;
                p = p.next;
            } else {
                a[i] = 0;
            }
            i--;
        }
        return a;
    }

    void insert(int k, int pos) {
        Elem p = head;
        Elem prev = new Elem();
        while (p != null && p.index > pos) {
            prev = p;
            p = p.next;
        }
        if (p == null || p.index < pos) {
            Elem q = new Elem();
            q.value = k;
            q.index = pos;
            q.next = p;
            prev.next = q;
        } else {
            p.value = k;
        }
    }

    void delete(int pos) {
        Elem p = head;
        Elem prev = new Elem();
        while (p != null && p.index > pos) {
            prev = p;
            p = p.next;
        }
        if (p != null && p.index == pos) {
            prev.next = p.next;
        }
    }

    int scalarProduct(VectorCode v) {
        int sp = 0;
        Elem p1 = head;
        Elem p2 = v.head;
        while (p1 != null && p2 != null) {
            if (p1.index > p2.index) {
                p1 = p1.next;
            } else if (p1.index < p2.index) {
                p2 = p2.next;
            } else {
                sp += p1.value * p2.value;
                p1 = p1.next;
                p2 = p2.next;
            }
        }
        return sp;
    }

    VectorCode sum(VectorCode v) {
        Elem p1 = head;
        Elem p2 = v.head;
        int size = max(length, v.length);
        int[] a = new int[size];
        int i = size - 1;
        while (p1 != null && p2 != null) {
            if (p1.index > p2.index) {
                a[p1.index] = p1.value;
                p1 = p1.next;
            } else if (p1.index < p2.index) {
                a[p2.index] = p2.value;
                p2 = p2.next;
            } else {
                a[p1.index] = p1.value + p2.value;
                p1 = p1.next;
                p2 = p2.next;
            }
        }
        while (p2 != null) {
            a[p2.index] = p2.value;
            p2 = p2.next;
        }
        while (p1 != null) {
            a[p1.index] = p1.value;
            p1 = p1.next;
        }
        return new VectorCode(a);
    }

    VectorCode vectorSum() {
        Elem p = head;
        int[] a = new int[length + 1];
        a[0] = 0;
        int i = 1;
        int cur = 0;
        while (p != null) {
            if (p.index >= length - i) {
                cur += p.value;
                p = p.next;
            }
            a[i] = cur;
            i++;
        }
        while (i < length + 1) {
            a[i] = cur;
            i++;
        }
        return new VectorCode(a);
    }

    void mult(int a, int c) {
        Elem p = head;
        while (p != null) {
            if (p.value == a)
                p.value *= c;
            p = p.next;
        }
    }

    boolean equals(VectorCode vc) {
        if (vc.length == length) {
            Elem p1 = head;
            Elem p2 = vc.head;
            while (p1 != null && p2 != null) {
                if (p1.value != p2.value || p1.index != p2.index)
                    return false;
                p1 = p1.next;
                p2 = p2.next;
            }
            return p1 == null && p2 == null;
        }
        return false;
    }

    public void printVector() {
        int[] ar = decode();
        for (int i : ar) {
            System.out.print(i + " ");
        }
        System.out.println();
    }
}
