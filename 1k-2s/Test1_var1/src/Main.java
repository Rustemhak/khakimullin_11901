import java.io.*;
import java.util.*;
import java.util.stream.*;

import static java.lang.Math.*;

public class Main {
    public static void main(String[] args) throws IOException {
        File airlinesFile = new File("airlines.csv");
        File airportsFile = new File("airports100.csv");
        File flightsFile = new File("flights.csv");
        List<Flights> flightsList = new BufferedReader(new FileReader(flightsFile))
                .lines()
                .skip(1)
                .map(f -> new Flights(f.split(", ")))
                .collect(Collectors.toList());
        Scanner in = new Scanner(airlinesFile);
        String s[] = in.nextLine().split(",");
//        System.out.println(Arrays.toString(s));
        List<Airlines> airlinesList = new ArrayList<>();
        for (int i = 0; i < 12; i++) {
            s = in.nextLine().split(",");
            airlinesList.add(new Airlines(s));
        }

//        System.out.println(Arrays.toString(s));
//        List<Airlines> airlinesList = new BufferedReader(new FileReader(airlinesFile))
//                .lines()
//                .skip(1)
//                .map(a -> new Airlines(a.split(",")))
//                .collect(Collectors.toList());
        List<Airports> airportsList = new BufferedReader(new FileReader(airportsFile))
                .lines()
                .skip(1)
                .map(a -> new Airports(a.split(",")))
                .collect(Collectors.toList());
//        System.out.println(airlinesList.toString());
//        System.out.println(airportsList.toString());
//        System.out.println(flightsList.toString());
        System.out.println(getAirline(airlinesList, flightsList));
        System.out.println(countFromFlights(airportsList, flightsList));
        System.out.println();
    }

    public static long airlineMostFlights(List<Airlines> airlinesList, List<Flights> flights) {
        return airlinesList.stream().map(a -> countFlights(flights, a.getId()))
                .reduce(Long::max).orElse((long) -1);
    }

    public static long countFlights(List<Flights> flights, int id) {
        return flights.stream().filter(f -> f.getAirline() == id).count();
    }

    public static List<String> getAirline(List<Airlines> airlinesList, List<Flights> flights) {
        return airlinesList.stream().
                filter(a -> countFlights(flights, a.getId()) == airlineMostFlights(airlinesList, flights)).
                map(Airlines::getAirline).collect(Collectors.toList());
    }

    //    public static long countFromFlights(List<Airports> airlinesList, List<Flights> flights) {
//        return flights.stream().
//                filter()
//    }
    public static List<Long> countFromFlights(List<Airports> airportsList, List<Flights> flights) {
        return airportsList.stream().map(a -> countForOne(a.getAirportCode(), flights)).collect(Collectors.toList());
    }

    public static long countForOne(String code, List<Flights> flights) {
        return flights.stream().
                filter(f -> f.getDestAirport().equals(code) || f.getSourceAirport().equals(code)).count();
    }
}
