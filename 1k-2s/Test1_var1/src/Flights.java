public class Flights {
    private int airline;
    private int flightNo;
    private String sourceAirport;
    private String destAirport;

//    Flights(int airline, int flightNo, String sourceAirport, String destAirport) {
//        this.airline = airline;
//        this.flightNo = flightNo;
//        this.sourceAirport = sourceAirport;
//        this.destAirport = destAirport;
//    }

    public Flights(String[] split) {
        this.airline = Integer.parseInt(split[0]);
        this.flightNo =  Integer.parseInt(split[1]);
        this.sourceAirport = split[2];
        this.destAirport = split[3];
    }

    public int getAirline() {
        return airline;
    }

    public String getSourceAirport() {
        return sourceAirport;
    }

    public String getDestAirport() {
        return destAirport;
    }
}
