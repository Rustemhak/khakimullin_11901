public class Airports {
    private String city;
    private String airportCode;
    private String airportName;
    private String country;
    private String countryAbbrev;

    Airports(String city, String airportCode, String airportName, String country, String countryAbbrev) {
        this.city = city;
        this.airportCode = airportCode;
        this.airportName = airportName;
        this.country = country;
        this.countryAbbrev = countryAbbrev;
    }

    public Airports(String[] split) {
        this.city = split[0];
        this.airportCode = split[1];
        this.airportName = split[2];
        this.country = split[3];
        this.countryAbbrev = split[4];
    }

    public String getAirportCode() {
        return airportCode;
    }
}
