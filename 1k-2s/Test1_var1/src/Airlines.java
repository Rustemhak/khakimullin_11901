public class Airlines {
    private int id;
    private String airline;
    private String abbreviation;
    private String country;

//    Airlines(int id, String airline, String abbreviation, String country) {
//        this.id = id;
//        this.airline = airline;
//        this.abbreviation = abbreviation;
//        this.country = country;
//    }

    public Airlines(String[] split) {
        this.id = Integer.parseInt(split[0]);
        this.airline = split[1];
        this.abbreviation = split[2];
        this.country = split[3];
    }

    public int getId() {
        return id;
    }

    public String getAirline() {
        return airline;
    }
}
