import java.util.*;
        import java.io.*;

public class MainArray {
    public static int ITERATION = 0;

    public static void main(String[] args) throws FileNotFoundException {
        int n = 0;
        Scanner in = new Scanner(new File("input.txt"));
        PrintWriter out = new PrintWriter(new File("outputArray.txt"));
        PrintWriter time = new PrintWriter(new File("timeArray.txt"));
        PrintWriter iterationsArray = new PrintWriter(new File("iterationsArray.txt"));
        PrintWriter sizesArray = new PrintWriter(new File("sizes.txt"));
        while (in.hasNext()){
            n = in.nextInt();
            int[] a = new int[n];
            MyArray array1 = new MyArray();
            for (int i = 0; i <n ; i++) {
                a[i]=in.nextInt();
            }
            ITERATION = 0;
            long before = System.nanoTime();
            int[] result = array1.sortArray(a);
            time.println(System.nanoTime() - before);
            iterationsArray.println(ITERATION);
            sizesArray.println(n);
            out.println(n);
            for (int i = 0; i < n; i++) {
                out.print(result[i] + " ");
            }
            out.println();
        }
        out.close();
        iterationsArray.close();
        time.close();
        sizesArray.close();
    }
}
