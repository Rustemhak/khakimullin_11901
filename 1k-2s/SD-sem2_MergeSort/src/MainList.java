import java.io.*;
import java.util.*;

public class MainList {
    public static int ITERATION;

    public static void main(String[] args) throws FileNotFoundException {
        Scanner in = new Scanner(new File("input.txt"));
        PrintWriter out = new PrintWriter(new File("outputList.txt"));
        PrintWriter time = new PrintWriter(new File("timeList.txt"));
        PrintWriter iterations = new PrintWriter(new File("iterationsList.txt"));
        int n;
        while (in.hasNext()) {
            n = in.nextInt();
            LinkedList<Integer> list = new LinkedList<>();
            MyLinkedList ml = new MyLinkedList();
            for (int i = 0; i < n; i++) {
                list.add(in.nextInt());
            }
            ITERATION = 0;
            long before = System.nanoTime();
            LinkedList<Integer> result = ml.sortList(list);
            time.println(System.nanoTime() - before);
            iterations.println(ITERATION);
            out.println(n);
            for (int i : result
            ) {
                out.print(i + " ");
            }
            out.println();
        }
        out.close();
        iterations.close();
        time.close();

    }
}
