import java.util.*;
import java.io.*;

public class GenerationElem {
    public static void main(String[] args) throws FileNotFoundException {
        PrintWriter generate = new PrintWriter(new File("input.txt"));
        Random random = new Random();
        int n = 0;
        while (n < 10000) {
            n += 100;
            generate.println(n);
            for (int i = 0; i < n; i++) {
                generate.print(random.nextInt() + " ");
            }
            generate.println();
        }
        generate.close();
    }
}
