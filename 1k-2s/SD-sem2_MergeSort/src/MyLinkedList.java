import java.net.Inet4Address;
import java.util.*;

public class MyLinkedList {
    public LinkedList<Integer> sortList(LinkedList<Integer> listA) { // сортировка Массива который передается в функцию
        // проверяем не нулевой ли он?
        if (listA == null) {
            return null;
        }
        // проверяем не 1 ли элемент в списке?
        if (listA.size() < 2) {
            return listA; // возврат в рекурсию в строки ниже см комменты.
        }
        // копируем левую часть от начала до середины
        LinkedList<Integer> listB = new LinkedList<>();
        copy(listA, 0, listB, listA.size() / 2);
        // копируем правую часть от середины до конца списка, вычитаем из длины первую часть
        LinkedList<Integer> listC = new LinkedList<>();
        copy(listA, listA.size() / 2, listC, listA.size());

        // рекурсией закидываем поделенные обе части обратно в наш метод, он будет крутится до тех пор,
        // пока не дойдет до 1 элемента в списке, после чего вернется в строку и будет искать второй такой же,
        // точнее правую часть от него и опять вернет его назад
        listB = sortList(listB); // левая часть возврат из рекурсии;
        listC = sortList(listC); // правая часть возврат из рекурсии строкой;
        // далее опять рекурсия возврата слияния двух отсортированных списокв
        return mergeList(listB, listC);
    }

    public static void copy(LinkedList<Integer> listA, int destPos, LinkedList<Integer> listB, int length) {
        for (int i = destPos; i < length; i++) {
            listB.add(listA.get(i));
        }
    }

    public LinkedList<Integer> mergeList(LinkedList<Integer> a1, LinkedList<Integer> a2) {
        //int[] b = new int[a1.length + a2.length];
        LinkedList<Integer> b = new LinkedList<>();
        int positionA1 = 0;
        int positionA2 = 0;
        int length = a1.size() + a2.size();
        for (int i = 0; i < length; i++) {
            if (positionA1 == a1.size()) {
                b.add(a2.get(positionA2));
                MainList.ITERATION += positionA2 + 1;
                positionA2++;
            } else if (positionA2 == a2.size()) {
                b.add(a1.get(positionA1));
                MainList.ITERATION += positionA1 + 1;
                positionA1++;
            } else {
                int elemA1 = a1.get(positionA1);
                int elemA2 = a2.get(positionA2);
                if (elemA1 < elemA2) {
                    b.add(elemA1);
                    positionA1++;
                } else {
                    b.add(elemA2);
                    positionA2++;
                }
                MainList.ITERATION += positionA1 + positionA2 + 2;
            }
        }
        return b;
    }
}
