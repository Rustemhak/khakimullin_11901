public class MyLinkedStack<T> implements IMyStack<T> {
    class Elem {
        T value;
        Elem next;
    }

    private Elem head = null;

    @Override
    public void push(T a) {
        Elem p = new Elem();
        p.value = a;
        p.next = head;
        head = p;
    }

    @Override
    public T pop() {
        T v = head.value;
        head = head.next;
        return v;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public T peek() {
        return head.value;
    }

    @Override
    public String toString() {
        String s = "";
        Elem p = head;
        while (p != null){
            s += p.value;
            p = p.next;
        }
        return s;
    }

}
