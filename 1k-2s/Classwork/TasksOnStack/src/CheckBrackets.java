import java.util.Scanner;
public class CheckBrackets {
    public static boolean checkWithStack(String s) {
        MyArrayStack<Character> stack = new MyArrayStack();
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '(') {
                stack.push('(');
            } else if (s.charAt(i) == ')') {
                if (stack.isEmpty()) {
                    return false;
                } else stack.pop();
            }
        }
        return stack.isEmpty();
    }

    public static boolean checkWithoutStack(String s) {
        int k = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '(')
                k++;
            else if (s.charAt(i) == ')')
                k--;
            if (k < 0)
                return false;
        }
        return k == 0;
    }

    public static boolean checkAnyBrackets(String s) {
        MyArrayStack<Character> stack = new MyArrayStack<>();
        int[][] a = {{'(', ')'}, {'[', ']'}, {'{', '}'}, {'<', '>'}};
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            boolean openBracket = false;
            for (int j = 0; j < 4 && !openBracket; j++) {
                if (c == a[j][0]) {
                    stack.push(c);
                    openBracket = true;
                }
            }
            if (!openBracket && !stack.isEmpty())
                for (int j = 0; j < 4; j++) {
                    if (c == a[j][1]) {
                        if (a[j][0] != stack.pop())
                            return false;
                        else break;
                    }
                }
            else if (stack.isEmpty())
                    return false;
        }
        return stack.isEmpty();
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s1 = in.nextLine();
        //3a and 3b
        System.out.println(checkWithStack(s1));
        System.out.println(checkWithoutStack(s1));
        //4
        String s2 = in.nextLine();
        System.out.println(checkAnyBrackets(s2));
    }
}
