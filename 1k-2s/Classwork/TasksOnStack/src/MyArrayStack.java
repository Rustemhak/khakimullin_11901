public class MyArrayStack<T> implements IMyStack<T> {
    private int size = 0;
    private int capacity = 1000;
    private T[] array;

    public MyArrayStack(int capacity) {
        this.capacity = capacity;
        array = (T[]) new Comparable[capacity];
        size = 0;
    }

    public MyArrayStack() {
        this(0);
    }

    @Override
    public void push(T a) {
        if (size >= capacity) {
            T[] newArray = (T[]) new Object[capacity * 3 / 2 + 1];
            for (int i = 0; i < size; i++) {
                newArray[i] = array[i];
            }
            capacity = capacity * 3 / 2 + 1;
            array = newArray;
        }
        array[size] = a;
        size++;
    }

    @Override
    public T pop() {
        size--;
        return array[size];
    }

    @Override
    public T peek() {
        return array[size - 1];
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

}
