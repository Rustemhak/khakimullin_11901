import java.util.Collection;

public interface IMyStack<T>{
    void push(T a);
    T pop();
    boolean isEmpty();
    T peek();
}
