/**
 * @author Khakimullin Rustem
 * 11-901
 * Task 04
 */

import java.util.*;

public class TaskForList {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Random r = new Random();
        int n = in.nextInt();
        Elem head = null, p;
        for (int i = 0; i < n; i++) {
            p = new Elem();
            p.value = r.nextInt(100);
            p.next = head;
            head = p;
        }
        //sort
        boolean b = false;
        while (!b) {
            p = head;
            b = true;
            while (p != null && p.next != null && b) {
                if (p.value > p.next.value) {
                    int temp = p.value;
                    p.value = p.next.value;
                    p.next.value = temp;
                    b = false;
                }
                p = p.next;
            }
        }
        System.out.println(toString(head));
        //insert
        p = head;
        while (p != null) {
            if (p.value % 2 == 0) {
                insert(p);
                p = p.next;
            }
            p = p.next;
        }
        System.out.println(toString(head));
        //delete
        p = head;
        int k = in.nextInt();
        while (p != null) {
            if (p.value % k == 0) {
                if (p.next != null) {
                    p.value = p.next.value;
                    p.next = p.next.next;
                } else p = null;
            } else
                p = p.next;
        }
        if (head.value % k == 0)
            head = null;

        System.out.println(toString(head));


    }

    public static void insert(Elem p) {
        Elem q = new Elem();
        q.value = 1;
        q.next = p.next;
        p.next = q;

        Elem c = new Elem();
        c.value = p.value;
        c.next = p.next;
        p.next = c;
        p.value = 0;

    }

    public static void delete(Elem p) {

    }

    public static void sort() {

    }


    public static String toString(Elem head) {
        Elem p = head;
        String str = "";
        while (p != null) {
            str += p.value + " ";
            p = p.next;
        }
        return str;
    }
}
