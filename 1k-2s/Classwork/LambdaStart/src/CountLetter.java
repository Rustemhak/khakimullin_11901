/**
 * @author Rustem Khakimullin
 * 11-901
 * Task 13
 */
import java.io.*;

import java.util.*;
import java.util.function.Function;
import java.util.stream.*;

public class CountLetter {
    public static void main(String[] args) throws FileNotFoundException {
        String s = "VivamusaeleifenderaDonec";

        Map<Character, Long> chars = s.chars().boxed().collect(Collectors.groupingBy((x) -> (char) (int) x, Collectors.counting()));

        for (Map.Entry<Character, Long> entry : chars.entrySet()
        ) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }
    }
}
