/**
 * @author Rustem Khakimullin
 * 11-901
 * Task 13
 */
import java.util.*;
import java.util.stream.*;

public class AverageLength {
    public static void main(String[] args) {
        ArrayList<String> strs = new ArrayList<>();
        strs.add("Anna");
        strs.add("Konstantin");
        strs.add("Damir");
        strs.add("Mikhail");
        double average =
                strs.stream()
                        .map((s) -> s.length())
                        .reduce(0, (s, s1) -> s + s1) * 1.0 / strs.size();
        System.out.println(average);
    }
}
