import java.lang.reflect.Array;
import java.util.*;
import java.util.stream.*;

public class Main {
    public static void main(String[] args) {
        ArrayList<String> strs = new ArrayList<>();
        strs.add("Anna");
        strs.add("Konstantin");
        strs.add("Damir");
        strs.add("Mikhail");
        Collections.sort(strs, (o1, o2) -> -1 * o1.compareTo(o2));
        //System.out.println(strs);
        //function
        List<Integer> lst = strs.stream().
                map(s->s.length()).
                collect(Collectors.toList());
        //procedure
        List<Integer> lst2 = new ArrayList<>();
        for (String s: strs
             ) {
            lst2.add(s.length());
        }
        int [] arr = {1,6,2,6,7,3,3};
        System.out.println(lst2);
        System.out.println(lst);
    }
}
