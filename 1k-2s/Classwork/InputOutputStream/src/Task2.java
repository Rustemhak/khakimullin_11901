import java.io.*;
import java.util.*;

public class Task2 {
    public static void main(String[] args) throws IOException {
        InputStream in = new FileInputStream("out1.txt");
        OutputStream out = new FileOutputStream("out2.txt");
        while (in.available() > 0) {
            out.write(in.read());
        }
        out.close();
    }
}
