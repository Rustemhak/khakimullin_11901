import java.io.*;
import java.util.*;

public class Main {
    public static void main(String[] args) throws IOException {
        OutputStream out = new FileOutputStream("out1.txt");
        Random r = new Random();
        int n = r.nextInt(11) + 10;
        for (int i = 0; i <n ; i++) {
            out.write(r.nextInt(101));
        }
        out.close();
    }
}
