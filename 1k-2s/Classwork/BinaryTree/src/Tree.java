import java.util.Scanner;

public class Tree<T> {

    private Node<T> root;

    public Tree(Node<T> root) {
        this.root = root;
    }

    public static void printTree(Node<?> p, int d) {
        if (p != null) {
            printTree(p.right, ++d);
            for (int i = 0; i < 2 * d; i++) {
                System.out.print(" ");
            }
            System.out.println(p.value);
            printTree(p.left, ++d);

        }
    }

    public static void balance(int n, Node<Integer> root) {
        Scanner in = new Scanner(System.in);
        if (n > 2) {
            root.left = new Node<>();
            root.right = new Node<>();
            int nl = (n - 1) / 2;
            int nr = n - 1 - nl;
            root.left.value = nl;
            root.right.value = nr;
            balance(nl, root.left);
            balance(nr, root.right);
        } else if (n > 1) {
            root.left = new Node<>();
            int nl = n / 2;
            root.left.value = nl;
        }
    }

    public static int eval(Node<String> v) {
        if (v.left != null && v.right != null) {
            String operation = v.value;
            switch (operation) {
                case "+":
                    return eval(v.left) + eval(v.right);
                case "-":
                    return eval(v.left) - eval(v.right);
                case "*":
                    return eval(v.left) * eval(v.right);
                case "/":
                    return eval(v.left) / eval(v.right);
            }
        }
        return Integer.parseInt(v.value);
    }

    public static void treeEx(String[] s, Node<String> root) {
        boolean[] used = new boolean[s.length];
        boolean first = false;
        for (int i = 0; i < s.length; i++) {
            String op = s[i];
            if (op.equals("-") || op.equals("+") || op.equals("*") || op.equals("/")) {
                if (!first) {
                    root.value = op;
                    first = true;
                }
                int k = 0;
                for (int j = i - 1; j > -1 && k < 2; j--) {
                    if (!used[i]) {
                        used[i] = true;
                    }
                }
            }
        }
    }
    // 1. balance tree where n vertex
    // 2. counting tree - eval
    // 3.* из постфиксной составить дерево выражений
}
    