import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        //balancing tree
        Node<Integer> root = new Node<>();
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        root.value = n;
        Tree.balance(n, root);
        Tree.printTree(root, 0);
        // Tree.printTree(n,0);
        //counting tree
        Node<String> n1 = new Node<>();
        n1.value = "+";
        Node<String> n2 = new Node<>();
        n2.value = "-";
        Node<String> n3 = new Node<>();
        n3.value = "*";
        Node<String> n4 = new Node<>();
        n4.value = "2";
        Node<String> n5 = new Node<>();
        n5.value = "-1";
        Node<String> n6 = new Node<>();
        n6.value ="3";
        Node<String> n7 = new Node<>();
        n7.value = "/";
        Node<String> n8 = new Node<>();
        n8.value = "8";
        Node<String> n9 = new Node<>();
        n9.value = "4";

        n1.left = n2;
        n1.right = n3;
        n2.left = n4;
        n2.right = n5;
        n3.left = n6;
        n3.right = n7;
        n7.left = n8;
        n7.right = n9;
        System.out.println(Tree.eval(n1));
    }
}
