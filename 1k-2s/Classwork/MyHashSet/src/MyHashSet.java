import java.util.*;

public class MyHashSet<T> implements Set<T> {
    private LinkedList<T>[] hashtable;
    private int size;

    public MyHashSet() {
        hashtable = new LinkedList[10000];
        size = 0;
        for (int i = 0; i < 10000; i++) {
            hashtable[i] = new LinkedList<>();
        }
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        int hashCode = o.hashCode();
        return hashtable[hashCode % 10000].contains(o);
    }

    @Override
    public Iterator<T> iterator() {
        LinkedList<T> ll = new LinkedList<>();
        for (LinkedList<T> ts : hashtable) {
            ll.addAll(ts);
        }
        return ll.iterator();
    }

    @Override
    public Object[] toArray() {
        LinkedList<T> ll = new LinkedList<>();
        for (LinkedList<T> ts : hashtable) {
            ll.addAll(ts);
        }
        return ll.toArray();
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        LinkedList<T> ll = new LinkedList<>();
        for (LinkedList<T> ts : hashtable) {
            ll.addAll(ts);
        }
        return ll.toArray(a);
    }

    @Override
    public boolean add(T t) {
        if (!contains(t)) {
            hashtable[t.hashCode() % 10000].add(t);
            size++;
            return true;
        }
        return false;
    }

    @Override
    public boolean remove(Object o) {
        if(contains(o))
            size--;
        return hashtable[o.hashCode() % 10000].remove(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object ts : c) {
            if (!contains(ts))
                return false;
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        for (T ts : c) {
            add(ts);
        }
        return true;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        for (Object ts : c
        ) {
            if (!contains(ts))
                remove(ts);
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        for (Object ts : c
        ) {
            remove(ts);
        }
        return true;
    }

    @Override
    public void clear() {
        for (LinkedList<T> list : hashtable
        ) {
            list.clear();
        }

    }
}
