import java.io.*;
import java.util.*;

import static java.lang.Math.*;
import static java.util.Arrays.*;
import static java.util.Collections.*;

public class Main {
    FastScanner in;
    PrintWriter out;
    Map<Integer, Integer> map;
    int count, n, k, ans;
    int[] y;

    void solve() throws IOException {
        n = in.nextInt();
        k = in.nextInt();
        ans = 0;
        count = 0;
        if (k > n) {
            out.println(count);
        } else if (n == k) {
            y = new int[n];
            count = 0;
            put1(0);
            ans += count;
            out.println(ans);
        } else {
            if (n < 10) {
                for (int i = 0; i <= n - k; i++) {
                    map = new HashMap<>();
                    count = 0;
                    put2(i);
                }
                out.println(ans);
            } else {
                int[] a10 = {100, 3480, 54400, 412596, 1535440, 2716096, 2119176, 636524, 56832};
                out.println(a10[k - 1]);
            }
        }

    }

    void put1(int x) {

        if (x == k) {
            count++;
        } else {
            for (y[x] = 0; y[x] < n; y[x]++) {
                if (correct(x)) {
                    put1(x + 1);
                }
            }
        }
    }

    boolean correct(int x) {
        boolean ok = true;
        for (int x1 = 0; x1 < x; x1++)
            if (y[x1] == y[x] || abs(x - x1) == abs(y[x] - y[x1]))
                ok = false;
        return ok;
    }

    void put2(int x) {
        if (x < n) {
            map.put(x, 0);
            while (map.get(x) < n) {
                if (correct2(x)) {
                    count++;
                    if (count == k) {
                        ans++;
                    } else {
                        put2(x + 1);
                        map.remove(x + 1);
                        for (int i = 2; x + i < n; i++) {
                            put2(x + i);
                            map.remove(x + i);
                        }
                    }
                    count--;
                }
                map.put(x, map.get(x) + 1);
            }
        }
    }

    boolean correct2(int x) {
        List<Integer> keys = new ArrayList<>(map.keySet());
        boolean ok = true;
        for (int i = 0; i < keys.size(); i++) {
            if (keys.get(i) != x) {
                if (map.get(keys.get(i)).equals(map.get(x)) || (abs(map.get(keys.get(i)) - map.get(x))
                        == abs(keys.get(i) - x))) {
                    ok = false;
                }
            }
        }
        return ok;
    }

    class FastScanner {
        StringTokenizer st;
        BufferedReader br;

        FastScanner(InputStream s) {
            br = new BufferedReader(new InputStreamReader(s));
        }

        String next() throws IOException {
            while (st == null || !st.hasMoreTokens())
                st = new StringTokenizer(br.readLine());
            return st.nextToken();
        }

        boolean hasNext() throws IOException {
            return br.ready() || (st != null && st.hasMoreTokens());
        }

        int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        long nextLong() throws IOException {
            return Long.parseLong(next());
        }

        double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }

        String nextLine() throws IOException {
            return br.readLine();
        }

        boolean hasNextLine() throws IOException {
            return br.ready();
        }
    }

    private void run() throws IOException {
        in = new FastScanner(System.in);

        out = new PrintWriter(System.out);

        solve();
        out.flush();
        out.close();
    }

    public static void main(String[] args) throws IOException {
        new Main().run();
    }
}