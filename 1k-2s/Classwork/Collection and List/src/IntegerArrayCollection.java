import java.util.Collection;
import java.util.Iterator;

// deadline - Feb, 14, 2020, 12:00
public class IntegerArrayCollection implements Collection<Integer> {

    private int[] array;
    private int size;
    private int capacity;

    // private IntegerArrayCollection head;
    // private IntegerArrayCollection next;
    public IntegerArrayCollection() {
        capacity = 1000;
        array = new int[capacity];
        size = 0;

    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return (size == 0);
    }

    // +
    @Override
    public boolean contains(Object o) {
        for (int i : array)
            if (o.equals(i))
                return true;
        return false;
    }

    // -
    @Override
    public Iterator<Integer> iterator()  {
        //Integer  next();
        //boolean hasNext();
        return null;
    }

    // +
    @Override
    public Object[] toArray() {
        Integer[] ar = new Integer[size];
        for (int i = 0; i < size; i++) {
            ar[i] = array[i];
        }
        return ar;
    }

    // -
    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean add(Integer x) {
        if (size >= capacity) {

            int newArray[] = new int[(capacity * 3) / 2 + 1];
            for (int i = 0; i < capacity; i++) {
                newArray[i] = array[i];
            }
            array = newArray;
            capacity = (capacity * 3) / 2 + 1;
        }
        array[size] = x;
        size++;
        return true; // capacity?????
    }

    public void remEl(int i) {
        for (int j = i; j < size - 1; j++) {
            array[i] = array[i + 1];
        }
        size--;
    }

    // +
    @Override
    public boolean remove(Object o) {
        for (int i = 0; i < size; i++) {
            if (o.equals(array[i])) {
                remEl(i);
                return true;
            }
        }
        return false;
    }

    // + (contains)
    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object i : c.toArray())
            if (!contains(i))
                return false;
        return true;
    }

    // + (add)
    @Override
    public boolean addAll(Collection<? extends Integer> c) {
        for (Object i : c.toArray()) //Object
            this.add((Integer) i);
        return true;
    }

    // + (remove)
    @Override
    public boolean removeAll(Collection<?> c) {
        boolean b = false;
        for (Object i : c.toArray())
            if (remove(i))
                b = true;
        return false;
    }

    // +
    @Override
    public boolean retainAll(Collection<?> c) {
        boolean b = false;
        for (int i = 0; i < size; i++) {
            boolean find = false;
            for (Object j : c.toArray()) {
                if (j.equals(array[i])) {
                    find = true;
                    break;
                }
            }
            if (!find) {
                remEl(i);
                b = true;
            }
        }
        return b;
    }

    // +
    @Override
    public void clear() {
        size = 0;
    }

    public int[] getArray() {
        return array;
    }

    public void setArray(int[] array) {
        this.array = array;
    }


}