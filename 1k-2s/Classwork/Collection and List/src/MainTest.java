import java.util.Arrays;

public class MainTest {
    public static void main(String[] args) {
        Test a = new Test();
        a.add(10);
        a.add(20);
        Test b = new Test();
        b.add(10);
        b.add(20);
        a.addAll(b);
        System.out.println(Arrays.toString(a.toArray()));
    }
}
