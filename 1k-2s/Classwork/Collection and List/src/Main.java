import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public class Main {
    public static void main(String[] args) {
        /*ArrayList<Integer> al = new ArrayList<>();
        al.add(1);
        System.out.println(Arrays.toString(al.toArray()));*/
        IntegerArrayCollection a = new IntegerArrayCollection();
        a.add(10);
        a.add(20);
        System.out.println(a.size());
        a.remove(20);
        IntegerArrayCollection a2 =
                new IntegerArrayCollection();
        a2.add(30);
        a.addAll(a2);
        System.out.println(Arrays.toString(a.toArray()));
        System.out.println(a.remove(10));
        System.out.println(Arrays.toString(a.toArray()));
        System.out.println(a.retainAll(a2));
        System.out.println(Arrays.toString(a.toArray()));
        System.out.println(a.containsAll(a2));
        System.out.println(a.removeAll(a2));
        System.out.println(Arrays.toString(a.toArray()));
        int[] c = {1, 2 ,3 ,4};
        System.out.println(a.contains(10));
    }
}