import java.util.*;
import java.io.*;

public class Task3 {
    public static void main(String[] args) throws IOException {
        Random r = new Random();
        int n = r.nextInt(5) + 3;
        byte[] ar = new byte[n];
        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream("1.txt")));
        PrintWriter pw = new PrintWriter(new FileOutputStream("2.txt"));
       // br.mark(n);
        for (int i = 0; i < n; i++) {
            ar[i] = (byte) br.read();
        }
        //br.reset();
        for (int i = 0; i < n; i++) {
            pw.write(ar[i]);
        }

        pw.close();
    }
}
