import java.io.*;
import java.nio.charset.StandardCharsets;

public class Main {


    public static void main(String [] args) throws IOException {

        // InputStream / OutputStream

        // Reader / Writer

        Writer w = new OutputStreamWriter(
                new FileOutputStream("1.txt")
        );
        w.write('b'); // 1
        w.write('d'); // 1
        w.write('я'); // 2
        w.write('ю'); // 2
        w.write('1'); // 1
        w.close();
		/*
		Reader r = new InputStreamReader(
						new FileInputStream("1.txt"),
						StandardCharsets.UTF_16
						);
		System.out.println((char) r.read());
		System.out.println((char) r.read());
		System.out.println((char) r.read());
		*/
        BufferedReader br = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream("1.txt")
                ));

        //System.out.println(br.readLine());
        br.mark(5);
        System.out.println((char) br.read());
        System.out.println((char) br.read());
        System.out.println((char) br.read());
        br.reset();
        System.out.println((char) br.read());

        PrintWriter pw = new PrintWriter("3.txt");
        pw.print("hello ");
        pw.print("good bye");
        pw.flush();
        pw.print(".");
        pw.close();

        pw = new PrintWriter(new FileOutputStream("4.txt"), true);
        pw.println("Hello, ITIS"); //autoflush
        pw.println("Good bye, ITIS!");


    }


}