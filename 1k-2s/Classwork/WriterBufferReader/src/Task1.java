import java.io.*;
import java.util.*;

public class Task1 {
    public static void main(String[] args) throws IOException {
        Random random = new Random();
        int n = random.nextInt(11) + 10;
        Writer w = new OutputStreamWriter(
                new FileOutputStream("1.txt")
        );
        PrintWriter pw = new PrintWriter(new FileOutputStream("2.txt"), true);
        for (int i = 0; i < n; i++) {
            int a =random.nextInt(101);
            w.write(a);
            pw.write(a);
        }
        w.close();
        pw.close();
    }
}
