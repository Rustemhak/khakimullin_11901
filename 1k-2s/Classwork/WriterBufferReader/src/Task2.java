import java.io.*;
import java.util.*;
public class Task2 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream("2.txt")
                ));
        Writer w = new OutputStreamWriter(
                new FileOutputStream("3.txt")
        );
        PrintWriter pw = new PrintWriter(new FileOutputStream("4.txt"), true);
        br.mark(100);
        pw.write(br.readLine());
        pw.close();
        br.reset();
        w.write(br.readLine());
        w.close();
    }
}
