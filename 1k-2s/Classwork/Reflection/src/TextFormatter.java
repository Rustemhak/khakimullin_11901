
import java.io.*;

public class TextFormatter {
    private Converter converter;

    TextFormatter(Converter c) {
        this.converter = c;
    }
    public void format(BufferedReader br, PrintWriter pw) throws IOException {
        String s =br.readLine();
        while (s!=null){
            pw.println(converter.convert(s));
            pw.flush();
            s = br.readLine();
        }
    }
}
