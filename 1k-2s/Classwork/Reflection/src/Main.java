import java.io.*;
import java.util.*;

public class Main {
    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {
        Scanner in = new Scanner(System.in);
        Class<? extends Converter> c = (Class<? extends Converter>) Class.forName(in.nextLine());
        Converter conv = c.newInstance();
        TextFormatter tf = new TextFormatter(conv);
        BufferedReader br = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream("input.txt")
                )
        );
        PrintWriter pw= new PrintWriter(new FileOutputStream("output.html"),true);
        tf.format(br,pw);
        br.close();
        pw.close();
    }
}