abstract public class Converter {
    abstract String convert(String input);
}
