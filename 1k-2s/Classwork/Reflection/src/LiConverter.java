public class LiConverter extends Converter {
    @Override
    String convert(String input) {
        return "<li>" + "\n" + input + "\n" + "</li>";
    }
}
