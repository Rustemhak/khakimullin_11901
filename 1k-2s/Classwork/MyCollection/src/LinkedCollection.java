import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

public class LinkedCollection<T extends Comparable<T>> implements Collection<T> {
    class Elem {
        T value;
        Elem next;

        @Override
        public String toString() {
            return value.toString();
        }
    }

    public LinkedCollection() {

    }

    Elem head;

    @Override
    public int size() {
        int c = 0;
        Elem p = head;
        while (p != null) {
            c++;
            p = p.next;
        }
        return c;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public boolean contains(Object o) {
        for (T elem : this
        ) {
            if (elem.equals(o)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            Elem p = head;

            @Override
            public boolean hasNext() {
                return p != null;
            }

            @Override
            public T next() {
                Elem q = p;
                p = p.next;
                return q.value;
            }
        };
    }

    @Override
    public Object[] toArray() {
        Elem p = head;
        T[] a = (T[]) new Comparable[size()];
        for (int i = 0; i < size(); i++) {
            a[i] = p.value;
            p = p.next;
        }
        return a;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        Elem p = head;
        for (int i = 0; i < size(); i++) {
            a[i] = (T) p.value;
            p = p.next;
        }
        return a;
    }

    @Override
    public boolean remove(Object o) {
        Elem p = head;
        Elem previous = null;
        while (p != null) {
            if (p.value.equals(o)) {
                if (p.next != null) {
                    p.value = p.next.value;
                    p.next = p.next.next;
                } else if (previous != null) {
                    previous.next = null;
                }
                return true;
            }
            previous = p;
            p = p.next;
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object o : c
        ) {
            if (!this.contains(o))
                return false;
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        for (Object o : c
        ) {
            remove(o);
        }
        return true;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        for (Object o : c
        ) {
            if (!this.contains(o))
                remove(o);
        }
        return true;
    }

    @Override
    public void clear() {
        head = null;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        for (T a : c
        ) {
            add(a);
        }
        return true;
    }

    @Override
    public boolean add(T elem) {
        Elem p = new Elem();
        p.value = elem;
        p.next = head;
        head = p;
        return true;
    }

    @Override
    public String toString() {
        return Arrays.toString(this.toArray());
    }
}
