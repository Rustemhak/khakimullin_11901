import org.junit.Assert;
import org.junit.Test;

public class MyCollectionTest {
    ArrayCollection<Integer> ac = new ArrayCollection<>();
    LinkedCollection<String> lc = new LinkedCollection<>();

    @Test
    public void addIntegerToArrayCollection() {
        boolean before = ac.contains(10);
        ac.add(10);
        Assert.assertEquals(ac.contains(10) && !before, ac.add(10));
    }

    @Test
    public void removeIntegerFromLinkedCollection() {
        lc.add("A");
        boolean b = lc.remove("A");
        Assert.assertEquals(lc.contains("A"), b);
    }
}
/*
homework:
10, 15, 19, 35-36,37
 */
