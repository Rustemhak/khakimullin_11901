public class Main2 {
    public static void main(String[] args) {
        LinkedCollection<Integer> a = new LinkedCollection<>();
        a.add(20);
        a.add(30);
        System.out.println(a);
        a.remove(20);
        System.out.println(a);
        LinkedCollection<Integer> b = new LinkedCollection<>();
        b.add(10);
        b.add(20);
        b.addAll(a);
        System.out.println(b);
        b.removeAll(a);
        System.out.println(b);
    }
}
