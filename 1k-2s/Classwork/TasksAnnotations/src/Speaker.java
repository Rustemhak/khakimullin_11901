import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Speaker {
    public void speak(Speech s) throws InvocationTargetException, IllegalAccessException {
        Class<? extends Speech> classS = s.getClass();
        for (Method m : classS.getMethods()
        ) {
            for (Annotation a : m.getAnnotations()
            ) {
                if (a.annotationType() == Say.class) {
                    StringBuilder string = new StringBuilder(m.invoke(s).toString());
                    for (Annotation a1 : m.getAnnotations()
                    ) {
                        if (a1.annotationType() == Loud.class)
                            string = new StringBuilder(string.toString().toUpperCase());
                        else if (a1.annotationType() == Mayakovskiy.class)
                            string = new StringBuilder(string.toString().replaceAll(" ", "\n"));
                        else if (a1.annotationType() == NoEnglish.class) {
                            removeEnglishLetters(string);
                        }
                    }
                    System.out.println(string.toString());
                }
                break;
            }
        }
    }

    private void removeEnglishLetters(StringBuilder sb) {
        int i = 0;
//        for (int i = 0; i < sb.length(); i++) {
        while (i < sb.length()) {
            String s = sb.toString().toUpperCase();
            if (s.charAt(i) >= 'A' && s.charAt(i) <= 'Z')
                sb.deleteCharAt(i);
            else i++;
        }
    }
}

