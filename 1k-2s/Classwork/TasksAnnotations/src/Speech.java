import java.lang.reflect.InvocationTargetException;

public class Speech {
    public String sayNobody(){
        return "Vestibulum at ipsum dapibus, vestibulum.";
    }
    @Say
    @Loud
    public String sayFirst(){
        return "Ut vitae efficitur sapien, at.";
    }
    @Say
    @Mayakovskiy
    public String saySecond(){
        return "Suspendisse ac ex malesuada, ultrices.";
    }
    @Say
    @Loud
    @NoEnglish
    public String sayThird(){
        return "AБP akaд AБP a";
    }

    public static void main(String[] args) throws InvocationTargetException, IllegalAccessException {
        Speech speech = new Speech();
        (new Speaker()).speak(speech);
    }
}
