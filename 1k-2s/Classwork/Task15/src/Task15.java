/**
* @author Rustem Khakimullin
* 11-901
* Task15
*/
public class Task15{
	public static double exp(double x){
		final  double EPS = 1e-9;
		//System.out.println(EPS);
		int fact = 1;
		int i = 1;
		double deg = 1;
		double s = 1.0;
		double a = deg / fact;
		while (Math.abs(a) > EPS){
			deg *= x;
			fact *= i;
			a = deg / fact;
			s += a;
			i++;
		}
		return (s);
	}
}