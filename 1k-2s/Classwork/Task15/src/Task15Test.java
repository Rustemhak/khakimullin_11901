import org.junit.Assert;
import org.junit.Test;

public class Task15Test {
    final  double EPS = 1e-9;
    @Test
    public void ExpPosNumber() {

        Assert.assertTrue(Math.abs(Math.exp(0.5) - Task15.exp(0.5)) <= EPS);
    }
    @Test
    public  void ExpNegNumber(){
        Assert.assertTrue(Math.abs(Math.exp(-0.5) - Task15.exp(-0.5)) <= EPS);
    }
    @Test
    public  void ExpNegLargeNumber(){
        Assert.assertTrue(Math.abs(Math.exp(-0.99) - Task15.exp(-0.99)) <= EPS);
    }
    @Test
    public  void ExpPosLargeNumber(){
        Assert.assertTrue(Math.abs(Math.exp(0.999) - Task15.exp(0.999)) <= EPS);
    }
}
