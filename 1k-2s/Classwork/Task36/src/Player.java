/**
 * @author Rustem Khakimullin
 * 11-901
 * for Task 36
 */

import java.util.Random;

public class Player {

    private int hp;
    private String name;

    public Player(String name) {
        this.name = name;
        this.hp = 100;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getHp() {
        return hp;
    }

    public String getName() {
        return name;
    }

    public void hit(Player p, int dmg) throws Exception {
        Random r = new Random();
        int s = r.nextInt(100);
        if (dmg > 0 && dmg < 10) {
            if ((double) s <= (double) 100 / dmg)
                p.setHp(p.getHp() - dmg);
        } else throw new Exception("Incorrect input");
    }

}