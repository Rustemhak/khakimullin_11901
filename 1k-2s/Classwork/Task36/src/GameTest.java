import org.junit.Assert;
import org.junit.Test;

public class GameTest {
    Player p1 = new Player("Rustem");
    Player p2 = new Player("Damir");
    @Test
    public void hitCorrect() throws Exception {
        p1.hit(p2,1);
        Assert.assertEquals(p2.getHp(),99);
    }
    @Test(expected = Exception.class)
    public void hitNegIncorrect() throws Exception{
        p2.hit(p2,-1);
    }
    @Test(expected = Exception.class)
    public void hitPosInCorrect() throws Exception {
        p1.hit(p2,10);
    }
}
