import java.util.Collection;
import java.util.Iterator;
import java.util.Queue;

public class MyDoubleStackQueue<T> implements Queue<T> {
    MyArrayStack<T> ms = new MyArrayStack<>();
    MyArrayStack<T> ss = new MyArrayStack<>();

    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return ms.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

    @Override
    public boolean add(T t) {
        return false;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public boolean offer(T t) {
        ms.push(t);
        return true;
    }

    @Override
    public T remove() {
        while (!ms.isEmpty()) {
            ss.push(ms.pop());
        }
        if (ss.peek() == null) {
            throw new NullPointerException();
        }
        T a = ss.pop();
        while (!ss.isEmpty()) {
            ms.push(ss.pop());
        }
        return a;
    }

    @Override
    public T poll() {
        while (!ms.isEmpty()) {
            ss.push(ms.pop());
        }
        if (ss.peek() == null) {
            return null;
        }
        T a = ss.pop();
        while (!ss.isEmpty()) {
            ms.push(ss.pop());
        }
        return a;

    }

    @Override
    public T element() {
        while (!ms.isEmpty()) {
            ss.push(ms.pop());
        }
        if (ss.peek() == null) {
            throw new NullPointerException();
        }
        T a = ss.peek();
        while (!ss.isEmpty()) {
            ms.push(ss.pop());
        }
        return a;
    }

    @Override
    public T peek() {
        while (!ms.isEmpty()) {
            ss.push(ms.pop());
        }
        if (ss.peek() == null) {
            return null;
        }
        T a = ss.peek();
        while (!ss.isEmpty()) {
            ms.push(ss.pop());
        }
        return a;
    }
}
