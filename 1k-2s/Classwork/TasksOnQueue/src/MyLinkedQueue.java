import java.util.Collection;
import java.util.Iterator;
import java.util.Queue;

public class MyLinkedQueue<T> implements Queue<T> {
    class Elem {
        T value;
        Elem next;
    }

    private Elem head;
    private Elem tail;

    public MyLinkedQueue() {
        head = null;
        tail = null;
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

    @Override
    public boolean add(T t) {
        return false;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public boolean offer(T t) {
        Elem p = new Elem();
        p.value = t;
        if (isEmpty())
            head = p;
        else
            tail.next = p;
        tail = p;
        return true;
    }

    @Override
    public T remove() {
        Elem p = head;
        head = head.next;
        return p.value;
    }

    @Override
    public T poll() {
        if (head == null)
            return null;
        Elem p = head;
        head = head.next;
        return p.value;
    }

    @Override
    public T element() {
        return head.value;
    }

    @Override
    public T peek() {
        if (head == null)
            return null;
        return head.value;
    }
}
