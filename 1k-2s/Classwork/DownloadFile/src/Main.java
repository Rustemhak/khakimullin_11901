import java.io.*;
import java.util.*;
import java.net.*;

public class Main {
    public static void main(String[] args) throws MalformedURLException, IOException {
        BufferedInputStream br = new BufferedInputStream(new URL
                ("https://sun9-8.userapi.com/SbsRtEbGxPjRP7JjdgclFoOaiQpX-E76NaMBmw/hezMi1BJUDw.jpg")
                .openStream());
        BufferedOutputStream pw = new BufferedOutputStream(new FileOutputStream("code.jpg"));
        int line = br.read();
        while (line >= 0) {
            pw.write(line);
            line = br.read();
        }
        br.close();
        pw.close();
    }
}
