public class MyMath {
    public static int fact(int n) {
        if (n < 0) {
            throw new ArithmeticException("Negative number");
        }
        int p = 1;
        for (int i = 1; i <= n; i++) {
            p *= i;
        }
        return p;
    }
    public static int sqr(int n){
        return 0;
    }
}
