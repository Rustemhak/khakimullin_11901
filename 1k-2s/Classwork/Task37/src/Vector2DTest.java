import org.junit.Assert;
import org.junit.Test;

public class Vector2DTest {
    Vector2D v1 = new Vector2D(1.0,1.0);
    Vector2D v2 = new Vector2D(1.0,2.0);
    Vector2D v3 =  v1.add(v2);
    Vector2D v4 = v1.mult(2);
    Vector2D v5 = v1.sub(v2);
    double scalarProduct = v1.scalarProduct(v2);
    @Test
    public void add1Vector(){
        Assert.assertEquals(v3.getX(),2.0,0.0);
    }
    @Test
    public void add2Vector(){
        Assert.assertEquals(v3.getY(),3.0,0.0);
    }
    @Test
    public void multVector(){
        Assert.assertEquals(v4.getX(),2.0,0.0);
    }
    @Test
    public void mult2Vector(){
        Assert.assertEquals(v4.getY(),2.0,0.0);
    }
    @Test
    public void subVector(){
        Assert.assertEquals(v5.getX(),1.0,0.0);
    }
    @Test
    public void sub2Vector(){
        Assert.assertEquals(v5.getX(),0.0,0.0);
    }
}
