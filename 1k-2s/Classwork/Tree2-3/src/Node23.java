import java.util.*;
public class Node23<T> {
    ArrayList<Node23<T>> children;
    Node23<T> parent;
    T key1;
    T key2;
    public Node23(T value){
        this.key1 = value;
        children = new ArrayList<>();
    }
    public Node23(){
        children = new ArrayList<>();
    }
}
