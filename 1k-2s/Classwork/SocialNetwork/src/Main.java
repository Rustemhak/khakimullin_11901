import java.util.*;
import java.util.stream.*;

public class Main {
    public static void main(String[] args) {
        int n = 5;
        User[] u = new User[n];
        Message[] m = new Message[n];
        u[1] = new User("Rustem", "Kazan", 2001);
        u[2] = new User("Katya", "Kazan", 2001);
        u[3] = new User("Adelya", "Izhevsk", 2001);
        u[4] = new User("Elvira", "Mozhga", 2001);
        m[1] = new Message(u[1], u[2], "Hello", false, "11 may");
        m[2] = new Message(u[1], u[3], "What to do", false, "11 may");
        m[3] = new Message(u[1], u[4], "What don't u understand", false, "11 may");
        m[4] = new Message(u[2], u[3], "Hi", false, "10 may");
        List<Subscription> subscriptions = new ArrayList<>();
        Subscription s1 = new Subscription(u[2], u[1]);
        Subscription s2 = new Subscription(u[3], u[2]);
        subscriptions.add(s1);
        subscriptions.add(s2);
        List<User> users = new ArrayList<>(Arrays.asList(u).subList(1, n));
        List<Message> messages = new ArrayList<>(Arrays.asList(m).subList(1, n));
        System.out.println(countMessageFromKazan(messages));
        System.out.println(receivedMessageMoreSent(users, messages).toString());
        System.out.println(listAdultFollowersMoreOthersAndHomeTownFollowersMoreOthers(users
                , subscriptions).
                toString());
        System.out.println(listWhichReceiversHaveLess3Town(users, messages));
    }

    //1
    public static long countMessageFromKazan(List<Message> messages) {
        return messages.stream().filter(m -> m.getSender().getTown().equals("Kazan")).count();
    }

    //2
    public static List<String> receivedMessageMoreSent(List<User> users, List<Message> messages) {
        return users.stream().
                filter(u -> countSentMessage(u, messages) > countReceivedMessage(u, messages)).
                map(User::getName).
                collect(Collectors.toList());
    }

    public static long countReceivedMessage(User user, List<Message> messages) {
        return messages.stream().filter(u -> u.getReceiver().equals(user)).count();
    }

    public static long countSentMessage(User user, List<Message> messages) {
        return messages.stream().filter(u -> u.getSender().equals(user)).count();
    }

    //3
    public static List<String> listAdultFollowersMoreOthersAndHomeTownFollowersMoreOthers
    (List<User> users,
     List<Subscription> subscriptions) {
        return users.stream().
                filter(u ->
                        countAdultFollowers(u, subscriptions) > countNotAdultFollowers(u, subscriptions)
                                &&
                                countHomeTownFollowers(u, subscriptions) < countOtherTownFollowers(u, subscriptions)).
                map(User::getName).collect(Collectors.toList());
    }

    public static long countAdultFollowers(User user, List<Subscription> subscriptions) {
        return subscriptions.stream().
                filter(s -> s.getSub().equals(user) && s.getFollower().getAge() >= 18).
                count();
    }

    public static long countNotAdultFollowers(User user, List<Subscription> subscriptions) {
        return subscriptions.stream().
                filter(s -> s.getSub().equals(user) && s.getFollower().getAge() < 18).
                count();
    }

    public static long countHomeTownFollowers(User user, List<Subscription> subscriptions) {
        return subscriptions.stream().
                filter(s -> s.getSub().equals(user) && s.getFollower().getTown().equals(s.getSub().getTown())).
                count();
    }

    public static long countOtherTownFollowers(User user, List<Subscription> subscriptions) {
        return subscriptions.stream().
                filter(s -> s.getSub().equals(user) && !s.getFollower().getTown().equals(s.getSub().getTown())).
                count();
    }

    //4
    public static List<String> listWhichReceiversHaveLess3Town(List<User> users, List<Message> messages) {
        return users.stream().filter(u -> countTown(u, messages) <= 2).map(User::getName).
                collect(Collectors.toList());
    }

    public static long countTown(User user, List<Message> messages) {
        return messages.stream().filter(m1 -> m1.getSender().equals(user) && !m1.isRead()
                && allMessagesNotRead(messages, m1.getSender(), m1.getReceiver()))
                .map(m -> m.getReceiver().getTown()).distinct().count();
    }

    public static boolean allMessagesNotRead(List<Message> messages, User sender, User receiver) {
        return messages.stream().filter(m -> m.getSender().equals(sender) && m.getReceiver().equals(receiver))
                .noneMatch(Message::isRead);
    }
}
