public class Subscription {
    private User follower;
    private User sub;
    Subscription(User follower, User sub){
        this.follower = follower;
        this.sub = sub;
    }
    public User getFollower() {
        return follower;
    }

    public User getSub() {
        return sub;
    }
}
