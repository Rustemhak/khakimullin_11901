public class Message {
    private User sender;
    private User receiver;
    private String text;
    private boolean read;
    private String date;

    public User getSender() {
        return sender;
    }

    Message(User sender, User receiver, String text, boolean read, String date) {
        this.sender = sender;
        this.receiver = receiver;
        this.text = text;
        this.date = date;
        this.read = read;
    }

    public User getReceiver() {
        return receiver;
    }

    public boolean isRead() {
        return read;
    }
}
