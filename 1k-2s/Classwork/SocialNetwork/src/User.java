public class User {
    private String name;
    private String town;
    private int year;
    private int id;
    private static int countID = 0;
    private static final int curYear = 2020;
    User(String name, String town, int year) {
        this.name = name;
        this.town = town;
        this.year = year;
        countID++;
        this.id = countID;
    }

    public String getTown() {
        return town;
    }

    public int getId() {
        return id;
    }

    public boolean equals(User u) {
        return u.getId() == this.id;
    }

    public String getName() {
        return name;
    }

    public int getYear() {
        return year;
    }
    public int getAge(){
        return curYear - this.year;
    }

}
