import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        /*Elem head = null;
        //создать 1 элемент 901
        Elem p = new Elem();
        p.value = 901;
        p.next = head;
        head = p;
        //создать 2 элемент 11
        p = new Elem();
        p.value = 11;
        p.next = head;
        head = p;
        //создать 3 элемент 2020
        p = new Elem();
        p.value = 2020;
        p.next = head;
        head = p;
        // вывести на экран список:
        p = head;
        while (p != null) {            // i = 0
            //что-то делаем с p.value //while (i < n)
            p = p.next;               //i+=1
        }*/
        //1-й способ в обратном порядке через head только
        Elem head = null;
        //ввод
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        for (int i = 0; i < n; i++) {
            Elem p = new Elem();
            p.value = in.nextInt();
            p.next = head;
            head = p;
        }
        //вывод
        Elem p = head;
        String str = "";
        while (p != null) {// i = 0
            str += p.value + " ";
            //что-то делаем с p.value //while (i < n)
            p = p.next;               //i+=1
        }
        System.out.println(str);
        //сумма
        int sum = 0;
        p = head;
        while (p != null) {
            sum += p.value;
            p = p.next;
        }
        System.out.println(sum);
        p = head;
        boolean b = false;
        while (p != null && !b) {
            if (p.value % 2 == 0)
                b = true;
            p = p.next;
        }
        System.out.println(b);
        //2-й способ в обратном порядке через head и tail
        head = null;
        Elem tail = null;
        //Elem p;
        //ввод
        n = in.nextInt();
        p = new Elem();
        p.value = in.nextInt();
        head = p;
        tail = p;
        for (int i = 1; i < n; i++) {
            p = new Elem();
            p.value = in.nextInt();

            tail.next = p;
            tail = p;
        }
        //вывод
        p = head;
        str = "";
        while (p != null) {
            str += p.value + " ";
            p = p.next;
        }
        System.out.println(str);
        p = head;
        //сумма
        sum = 0;
        while (p != null) {
            sum += p.value;
            p = p.next;
        }
        System.out.println(sum);
        b = false;
        p = head;
        while (p != null && !b) {
            if (p.value % 2 == 0)
                b = true;
            p = p.next;
        }
        System.out.println(b);
    }

}
//hw: создание линейного одн. списка -в прямом(с хвостом) и обратном порядке и обратном порядке
//вывести список на экран
//сумма