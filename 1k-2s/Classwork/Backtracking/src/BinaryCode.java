import java.util.*;

public class BinaryCode {
    public static void main(String[] args) {
        String s = "";
        correct(s);
    }

    public static void correct(String s) {
        if (s.length() > 1) {
            if (s.charAt(s.length() - 1) != s.charAt(s.length() - 2)) {
                if (s.length() == 5) {
                    System.out.println(s);
                    return;
                }
                correct(s + "0");
                correct(s + "1");
            }
        }
        else {
            correct(s + "0");
            correct(s + "1");
        }
    }
}
