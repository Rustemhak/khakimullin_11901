import java.util.*;
import java.io.*;

public class GraphColoring {
    ArrayList<Integer>[] graph; // Граф храним в виде списка смежности
    boolean[] used;
    int[] color;
    Stack<Integer> path;

    public void solve() throws FileNotFoundException {
        Scanner in = new Scanner(new File("input.txt"));
        int n = in.nextInt(), m = in.nextInt();
        graph = new ArrayList[n];
        used = new boolean[n];
        path = new Stack<>();
        //0 - clear, 1 - red, 2 - yellow, 3 - green, 4 - blue
        color = new int[n];
        for (int i = 0; i < n; i++)
            graph[i] = new ArrayList();
        for (int i = 0; i < m; i++) {
            int v = in.nextInt() - 1, u = in.nextInt() - 1;
            graph[v].add(u);
            graph[u].add(v);
        }
        String[] colors = {"no", "red", "orange", "green", "blue"};
        //делаем обход в глубину, всех компонент связности
        for (int i = 0; i < n; i++)
            if (!used[i]) {
                path.clear();
                path.push(i);
                recDfs(i);
            }
        for (int i : color
        ) {
            System.out.print(colors[i] + " ");
        }
    }

    public void recDfs(int v) {
        int oldColor = color[v];
        boolean[] impColorCur = new boolean[5];
        for (int u : graph[v])
            impColorCur[color[u]] = true;
        for (int i = 1; i < 5; i++) {
            if (!impColorCur[i] && oldColor != i) {
                color[v] = i;
                break;
            }
        }
        if (oldColor == color[v]) {
            used[v] = false;
            color[v] = 0;
            recDfs(path.pop());
        } else {
            used[v] = true;
            for (int u : graph[v]) {
                if (!used[u]) {
                    if (path.peek() != v)
                        path.push(v);
                    recDfs(u);
                }
            }
        }
    }

    public static void main(String[] args) throws FileNotFoundException {
        new GraphColoring().solve();
    }
}
