import java.io.*;
import java.util.*;

public class ColoringTracking {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner in = new Scanner(new File("input.txt"));
        int n = in.nextInt();
        int[][] a = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                a[i][j] = in.nextInt();
            }
        }
        int[] color = new int[n];
        int i = 0;
        while (i < n) {
            boolean[] impColor = new boolean[5];
            int oldColor = color[i];
            for (int j = 0; j < n; j++) {
                if (a[i][j] == 1)
                    impColor[color[j]] = true;
            }
            for (int j = 1; j < 5; j++) {
                if (!impColor[j] && oldColor != j) {
                    color[i] = j;
                    break;
                }
            }
            if (color[i] == oldColor)
                i--;
            else i++;
        }
        String[] s = {"", "red", "yellow", "green", "blue"};
        for (int j : color
        ) {
            System.out.print(s[j] + " ");
        }
    }
}
