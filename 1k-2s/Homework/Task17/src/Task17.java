import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task17 {
    public static void main(String[] args) throws IOException, MalformedURLException {
        URL uMain = new URL("https://en.wikipedia.org/wiki/London");
        BufferedInputStream br = new BufferedInputStream(uMain
                .openStream());

        Pattern p = Pattern.compile("href=\"[^\"]+\\.[a-z]{3}\"");
        BufferedOutputStream pw = new BufferedOutputStream(new FileOutputStream("index.html"));
        int line = br.read();

        while (line >= 0) {
            pw.write(line);
            line = br.read();
        }
        br.close();
        pw.close();
        BufferedReader br1 = new BufferedReader(new InputStreamReader(new FileInputStream("index.html")));
        String line1 = br1.readLine();
        uMain = new URL("https://en.wikipedia.org");
        int k = 0;
        while (line1 != null) {
            Matcher m = p.matcher(br1.readLine());
            while (m.find()) {
                String s = m.group();
                String link = s.split("\"")[1];
                    if (link.charAt(0) == '/') {
                        link = uMain + link;
                    }
                String[] path = link.split("\\.");

                String format = path[path.length - 1];
                if (format.equals("org")||format.equals("cfm")) {
                    break;
                }
                k++;
                URL u = new URL(link);
                BufferedInputStream is = new BufferedInputStream(u.openStream());


                BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream(k + "." + format));
                line = is.read();
                while (line >= 0) {
                    os.write(line);
                    line = is.read();
                }
                os.flush();
                os.close();
                is.close();
            }
            line1 = br1.readLine();
        }
    }
}
