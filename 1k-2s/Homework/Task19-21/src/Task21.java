/**
 * @author Khakimullin Rustem
 * 11-901
 * Task 21
 */

import java.io.*;
import java.util.*;

public class Task21 {
    public static void main(String[] args) throws IOException {
        Random r = new Random();
        int size = r.nextInt(5) + 3;
        byte[] a = new byte[size];
        InputStream in = new FileInputStream("out1.txt");
        OutputStream out = new FileOutputStream("out3.txt");
        if (in.available() > 0) {
            for (int i = 0; i < size; i++) {
                a[i] = (byte) in.read();
            }
            for (int i = 0; i < size; i++) {
                out.write(a[i]);
            }
            while (in.available() > 0) {
                out.write(in.read());
            }
        }
    }
}

