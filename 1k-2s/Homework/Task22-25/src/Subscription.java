/**
 * @author Khakimullin Rustem
 * 11-901
 * for Task 22-25
 */

public class Subscription {
    private User follower;
    private User sub;
    Subscription(User follower, User sub){
        this.follower = follower;
        this.sub = sub;
    }
    public User getFollower() {
        return follower;
    }

    public User getSub() {
        return sub;
    }
}
