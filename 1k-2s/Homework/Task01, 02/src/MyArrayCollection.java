/**
 * @author Khakimullin Rustem
 * 11-901
 * Task 01
 */

import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;

// deadline - Feb, 14, 2020, 12:00
public class MyArrayCollection<T extends Comparable<T>> implements Collection<T> {

    private T[] array;
    private int size;
    private int capacity;
    //private int i;

    // private IntegerArrayCollection head;
    // private IntegerArrayCollection next;
    public MyArrayCollection(int capacity) {
        this.capacity = capacity;
        array = (T[]) new Comparable[capacity];
        size = 0;
    }

    public MyArrayCollection() {
        this(0);
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return (size == 0);
    }

    // +
    @Override
    public boolean contains(Object o) {
        for (Object i : array)
            if (o.equals(i))
                return true;
        return false;
    }

    // -
    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            int i = 0;

            @Override
            public boolean hasNext() {
                return i < size;
            }

            @Override
            public T next() {
                i++;
                return array[i - 1];
            }
        };
    }
    /*
    foreach T a : c:
    Iterator =  c.iterator();
    while hasnext():
     T a = next();
     ...
     */

    @Override
    public Object[] toArray() {
        Object[] a = new Object[size];
        for (int i = 0; i < size; i++) {
            a[i] = array[i];
        }
        return a;
    }

    // +

    @Override
    public <T> T[] toArray(T[] a) {
        for (int i = 0; i < size; i++) {
            a[i] = (T) array[i];
        }
        return a;
    }

    @Override
    public boolean add(T t) {

        if (size >= capacity) {
            T[] arr = (T[]) new Comparable[capacity * 3 / 2 + 1];
            for (int j = 0; j < size; j++) {
                arr[j] = array[j];
            }
            capacity = capacity * 3 / 2 + 1;

            arr[size] = t;
            array = arr;
            size++;
            return true;
        } else {
            array[size] = t;
            size++;
            return true;
        }
    }

    public void remEl(int i) {
        for (int j = i; j < size - 1; j++) {
            array[i] = array[i + 1];
        }
        size--;
    }

    // +
    @Override
    public boolean remove(Object o) {
        for (int i = 0; i < size; i++) {
            if (o.equals(array[i])) {
                remEl(i);
                return true;
            }
        }
        return false;
    }

    // + (contains)
    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object i : c.toArray())
            if (!contains(i))
                return false;
        return true;
    }

    // + (add)
    @Override
    public boolean addAll(Collection<? extends T> c) {
        for (T i : c) {
            add(i); //шаблонный метод
        }
        return true;
    }

    // + (remove)
    @Override
    public boolean removeAll(Collection<?> c) {
        boolean b = false;
        for (Object i : c.toArray())
            if (remove(i))
                b = true;
        return false;
    }

    // +
    @Override
    public boolean retainAll(Collection<?> c) {
        boolean b = false;
        for (int i = 0; i < size; i++) {
            boolean find = false;
            for (Object j : c.toArray()) {
                if (j.equals(array[i])) {
                    find = true;
                    break;
                }
            }
            if (!find) {
                remEl(i);
                b = true;
            }
        }
        return b;
    }

    public void sort() {
        for (int i = 1; i < size; i++) {
            for (int j = 0; j < size - i; j++) {
                if (array[j].compareTo(array[j + 1]) > 0) {
                    T temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
    }

    public void sort(Comparator<T> c) {
        for (int i = 1; i < size; i++) {
            for (int j = 0; j < size - i; j++) {
                if (c.compare(array[j], array[j + 1]) > 0) {
                    T temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
    }

    // +
    @Override
    public void clear() {
        size = 0;
    }

    public T[] getArray() {
        return array;
    }

    public void setArray(T[] array) {
        this.array = array;
    }

    @Override
    public String toString() {
        return Arrays.toString(toArray());
    }


}