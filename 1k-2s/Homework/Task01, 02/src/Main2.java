/**
 * @author Khakimullin Rustem
 * 11-901
 * for Task 02
 */

public class Main2 {
    public static void main(String[] args) {
        MyLinkedCollection<Integer> a = new MyLinkedCollection<>();
        a.add(20);
        a.add(30);
        System.out.println(a);
        a.remove(20);
        System.out.println(a);
        MyLinkedCollection<Integer> b = new MyLinkedCollection<>();
        b.add(10);
        b.add(20);
        b.addAll(a);
        System.out.println(b);
        b.removeAll(a);
        System.out.println(b);
    }
}
