/**
 * @author Khakimullin Rustem
 * 11-901
 *for Task 01
 */

import java.util.Comparator;
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        MyArrayCollection<User> users = new MyArrayCollection<>();
        users.add(new User("Rus", 2001, "Kaz"));
        users.add(new User("Dam", 2001, "Kaz"));
        users.add(new User("MM", 1989, "Kaz"));
        System.out.println(users);
        users.sort();
        System.out.println(users);
        users.sort(new Comparator<User>() {
            @Override
            public int compare(User o1, User o2) {
                return -1 * o1.compareTo(o2);
            }
        });
        System.out.println(users);
    }
}
