/**
 * @author Khakimullin Rustem
 * 11-901
 * for Task 01
 */

public class User implements Comparable<User> {
    private String username;
    private int birthday;
    private String city;

    public User(String username, int birthday, String city) {
        this.username = username;
        this.birthday = birthday;
        this.city = city;
    }

    @Override
    public int compareTo(User o) {
        return this.username.compareTo(o.getUsername());
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public int getBirthday() {
        return birthday;
    }

    public String getCity() {
        return city;
    }

    public void setBirthday(int birthday) {
        this.birthday = birthday;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "username=" + username + ", birthday=" + birthday + ", city = " + city;
    }
}
