/**
 * @author Khakimullin Rustem
 * 11-901
 * Task 09
 */
public class ReversePolishNotation {
    public static int countArithmeticEx(String s) {
        String[] str = s.split(" ");
        MyArrayStack<Integer> stack = new MyArrayStack<>();
        for (int i = 0; i < str.length; i++) {
            String c = str[i];
            switch (c) {
                case "*":
                    stack.push(stack.pop() * stack.pop());
                    break;
                case "/":
                    stack.push(stack.pop() / stack.pop());
                    break;
                case "+":
                    stack.push(stack.pop() + stack.pop());
                    break;
                case "-":
                    stack.push(stack.pop() - stack.pop());
                    break;
                default:
                    stack.push(Integer.parseInt(c));
                    break;
            }
        }
        return stack.pop();
    }

    public static void main(String[] args) {
        String s = "7 2 3 * -";
        System.out.println(countArithmeticEx(s));
    }
}
