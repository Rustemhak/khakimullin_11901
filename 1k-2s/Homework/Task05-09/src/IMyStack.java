/**
 * @author Khakimullin Rustem
 * 11-901
 *for Task 05, 06
 */

import java.util.Collection;

public interface IMyStack<T>{
    void push(T a);
    T pop();
    boolean isEmpty();
    T peek();
}
