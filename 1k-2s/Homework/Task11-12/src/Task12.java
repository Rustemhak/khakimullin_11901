/**
 * @author Khakimullin Rustem
 * 11-901
 * Task 12
 */

import java.io.*;
import java.util.*;

public class Task12 {
    public static void main(String[] args) throws FileNotFoundException {
        HashMap<Character, Integer> hm = new HashMap<>();

        Scanner in = new Scanner(new File("input.txt"));
        StringBuilder s = new StringBuilder();
        while (in.hasNext()) {
            s.append(in.nextLine());
        }
        s = new StringBuilder(s.toString().toUpperCase());
        for (int i = 0; i < 26; i++) {
            hm.put((char) ('A' + i), 0);
        }
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) >= 'A' && s.charAt(i) <= 'Z')
                hm.put(s.charAt(i), hm.get(s.charAt(i)) + 1);
        }
        for (int i = 0; i < 26; i++) {
            System.out.print(hm.get((char) ('A' + i)) + " ");
        }
    }
}
