/**
 * @author Khakimullin Rustem
 * 11-901
 * Task 11
 */

import java.io.*;
import java.util.*;

public class Task11 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner in = new Scanner(new File("ListOfNumber.txt"));
        HashMap<String, ArrayList<String>> hm = new HashMap<>();
        while (in.hasNextLine()) {
            String name = in.next();
            String number = in.next();
            if (!hm.containsKey(name)) {
                ArrayList<String> num = new ArrayList<>();
                num.add(number);
                hm.put(name, num);
            } else hm.get(name).add(number);
        }
        for (HashMap.Entry<String, ArrayList<String>> entry : hm.entrySet()
        ) {
            System.out.println(entry.getKey() + " " + Arrays.toString(entry.getValue().toArray()));
        }
    }
}
