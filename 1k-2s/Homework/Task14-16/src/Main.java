/**
 * @author Khakimullin Rustem
 * 11-901
 * Task 14-16
 */

import java.net.Inet4Address;
import java.util.*;
import java.util.stream.*;
import java.io.*;

import static java.util.stream.Collectors.*;

public class Main {
    public static void main(String[] args) throws IOException {
        File producersFile = new File("producers.txt");
        File actorsFile = new File("actors.txt");
        File filmsFile = new File("films.txt");
        File partsFile = new File("parts.txt");
        List<Producer> producers = new BufferedReader(new FileReader(producersFile))
                .lines().map(p -> new Producer(p.split(" ")))
                .collect(toList());
        List<Actor> actors = new BufferedReader(new FileReader(actorsFile))
                .lines().map(a -> new Actor(a.split(" ")))
                .collect(toList());
        List<Film> films = new BufferedReader(new FileReader(filmsFile))
                .lines().map(f -> new Film(f.split(" ")))
                .collect(toList());

        List<Part> parts = new BufferedReader(new FileReader(partsFile))
                .lines().map(p -> new Part(p.split(" ")))
                .collect(toList());
        Scanner in = new Scanner(System.in);
        //14
        int idProducer = in.nextInt();
        int year = in.nextInt();
        //stream api
        System.out.println(listProducerWhichMadeBeforeYear1(films, idProducer, year));
        //procedure
        System.out.println(listProducerWhichMadeBeforeYear2(films, idProducer, year));
        //15
        //stream api
        System.out.println(countProducerActorTogether1(parts, films));
        //procedure
        System.out.println(countProducerActorTogether2(parts, films));
        //16
        int x = in.nextInt();
        //stream api
        System.out.println(listActorAfterXYearCastOfOneProducer(actors,parts,x,films));
        //procedure
        System.out.println(setActorAfterXYearCastOfOneProducer2(parts, x, films));
    }

    //14 stream api
    public static List<String> listProducerWhichMadeBeforeYear1(List<Film> films, int idProducer, int year) {
        return films.stream().filter(f -> f.getProducerId() == idProducer && f.getYear() < year)
                .map(Film::getName).collect(toList());
    }

    //14 procedure
    public static List<String> listProducerWhichMadeBeforeYear2(List<Film> films, int idProducer, int year) {
        List<String> filmsAns = new ArrayList<>();
        for (Film film : films) {
            if (film.getProducerId() == idProducer && film.getYear() < year)
                filmsAns.add(film.getName());
        }
        return filmsAns;
    }

    //15 stream api
    public static Map<String, Long> countProducerActorTogether1(List<Part> parts, List<Film> films) {
        return parts.stream()
                .collect(groupingBy(p -> producerIdOfFilm1(p.getFilmId(), films) + "-" + p.getActorID(), counting()));
    }

    public static int producerIdOfFilm1(int filmId, List<Film> films) {
        return films.stream().filter(f -> f.getId() == filmId).map(Film::getProducerId).findAny().orElse(0);
    }

    //15 procedure
    public static Map<String, Long> countProducerActorTogether2(List<Part> parts, List<Film> films) {
        Map<String, Long> mapAns = new TreeMap<>();
        for (Part part : parts) {
            int producerId = producerIdOfFilm2(part.getFilmId(), films);
            if (producerId != 0) {
                String key = producerId + "-" + part.getActorID();
                if (mapAns.containsKey(key)) {
                    mapAns.put(key, mapAns.get(key) + 1);
                } else mapAns.put(key, 1L);
            }
        }
        return mapAns;
    }

    public static int producerIdOfFilm2(int filmId, List<Film> films) {
        for (Film film : films)
            if (film.getId() == filmId)
                return film.getProducerId();
        return 0;
    }

    //16 stream api
    public static List<Integer> listActorAfterXYearCastOfOneProducer
    (List<Actor> actors, List<Part> parts, int x, List<Film> films) {
        return actors
                .stream()
                .filter(a -> countAfterXYearProducer(parts, films, a.getId(), x) == 1)
                .map(Actor::getId)
                .collect(toList());
    }

    public static long countAfterXYearProducer(List<Part> parts, List<Film> films, int actorID, int x) {
        return parts
                .stream()
                .filter(p -> p.getActorID() == actorID
                        && producerIdOfFilmWithYear1(p.getFilmId(), films, x) != 0)
                .count();
    }

    public static long producerIdOfFilmWithYear1(int filmId, List<Film> films, int x) {
        return films.stream().filter(f -> f.getId() == filmId && f.getYear() > x)
                .map(Film::getProducerId)
                .findAny()
                .orElse(0);

    }

    //16 procedure
    public static Set<Integer> setActorAfterXYearCastOfOneProducer2(List<Part> parts, int x, List<Film> films) {
        Map<Integer, Integer> mapActors = new TreeMap<>();
        for (Part part : parts) {
            int actorID = part.getActorID();
            int filmId = part.getFilmId();
            int producerId = producerIdOfFilmWithYear2(filmId, films, x);
            if (producerId != 0) {
                if (mapActors.containsKey(actorID) &&
                        mapActors.get(actorID) != producerId)
                    mapActors.remove(actorID);
                else mapActors.put(actorID, producerId);
            }
        }
        return mapActors.keySet();
    }

    public static int producerIdOfFilmWithYear2(int filmId, List<Film> films, int x) {
        for (Film film : films)
            if (film.getId() == filmId && film.getYear() > x)
                return film.getProducerId();
        return 0;
    }
}
