/**
 * @author Khakimullin Rustem
 * 11-901
 * for Task 14-16
 */

public class Film {
    private String name;
    private int id;
    private int year;
    private int producerId;

    public Film(String[] s) {
        name = s[0];
        id = Integer.parseInt(s[1]);
        year = Integer.parseInt(s[2]);
        producerId=  Integer.parseInt(s[3]);
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public int getYear() {
        return year;
    }

    public int getProducerId() {
        return producerId;
    }
}
