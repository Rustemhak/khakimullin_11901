/**
 * @author Khakimullin Rustem
 * 11-901
 * for Task 14-16
 */

public class Part {
    private int filmId;
    private int actorID;
    private String name;

    public Part(String[] s) {
        filmId = Integer.parseInt(s[0]);
        actorID = Integer.parseInt(s[1]);
        name = s[2];
    }

    public int getFilmId() {
        return filmId;
    }

    public int getActorID() {
        return actorID;
    }

    public String getName() {
        return name;
    }
}
