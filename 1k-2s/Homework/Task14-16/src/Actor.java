/**
 * @author Khakimullin Rustem
 * 11-901
 * for Task 14-16
 */

public class Actor {
    private String fullName;
    private int id;
    private int seniority;

    public Actor(String[] s) {
        fullName = s[0] + " " + s[1];
        id = Integer.parseInt(s[2]);
        seniority = Integer.parseInt(s[3]);
    }

    public String getFullName() {
        return fullName;
    }

    public int getId() {
        return id;
    }

    public int getSeniority() {
        return seniority;
    }
}
