/**
 * @author Khakimullin Rustem
 * 11-901
 * for Task 14-16
 */

public class Producer {
    private String fullName;
    private int id;

//    Producer(String fullName,int id) {
//        this.fullName = fullName;
//        this.id = id;
//    }

    public Producer(String[] s) {
        this.fullName = s[0] + " " + s[1];
        this.id = Integer.parseInt(s[2]);
    }
}
