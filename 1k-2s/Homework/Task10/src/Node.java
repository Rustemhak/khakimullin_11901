/**
 * @author Rustem Khakimullin
 * 11-901
 * for Task 10
 */
class Node<T> {
    T value;
    Node<T> left, right;
}
