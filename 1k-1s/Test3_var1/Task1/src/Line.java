public class Line {
    private double x1, y1, x2, y2;

    public Line(double x1, double y1, double x2, double y2) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
    }

    public Line() {
        this(0, 0, 0, 0);
    }

    public double length() {
        return Math.sqrt((this.x1 - this.x2) * (this.x1 - this.x2) + (this.y1 - this.y2) * (this.y1 - this.y2));
    }

    public double sqrRec() {
        return (Math.abs(this.x1 - this.x2) * Math.abs(this.y1 - this.y2));
    }

    public boolean intersection(Line l) {
        double v1 = (l.getX2() - l.getX1()) * (this.y1 - l.getY1()) - (l.getY2() - l.getY1()) * (this.x1 - l.getX1());
        double v2 = (l.getX2() - l.getX1()) * (this.y2 - l.getY1()) - (l.getY2() - l.getY1()) * (this.x2 - l.getX1());
        double v3 = (this.x2 - this.x1) * (l.getY1() - this.y1) - (this.y2 - this.y1) * (l.getX1() - this.x1);
        double v4 = (this.x2 - this.x1) * (l.getY2() - this.y1) - (this.y2 - this.y1) * (l.getX2() - this.x1);
        if (v1 * v2 < 0 && v3 * v4 < 0)
            return true;
        else return false;
    }

    public double getX1() {
        return x1;
    }

    public double getX2() {
        return x2;
    }

    public double getY1() {
        return y1;
    }

    public double getY2() {
        return y2;
    }

    @Override
    public String toString() {
        return "(" + this.x1 + ";" + this.y1 + ")" + "," + "(" + this.x2 + ";" + this.y2 + ")";
    }

    //@Override
    public boolean equals(Line l) {
        if (this.x1 == l.getX1() && this.x2 == l.getX2() && this.y1 == l.getY1() && this.y2 == l.getY2())
            return true;
        else return false;
    }
}
