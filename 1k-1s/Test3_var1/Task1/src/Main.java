import java.util.Scanner;

public class Main {
    public static double comLen(Line[] l) {
        double s = 0;
        for (int i = 0; i < l.length; i++) {
            s += l[i].length();
        }
        return s;
    }

    public static boolean comPoint(Line[] l) {
        for (int i = 0; i < l.length - 1; i++) {
            if (l[i].equals(l[i + 1]))
                return true;
        }
        return false;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt(); // кол-во отрезков
        Line[] l = new Line[n];
        for (int i = 0; i < n; i++) {
            double x1 = in.nextDouble(), y1 = in.nextDouble(), x2 = in.nextDouble(), y2 = in.nextDouble();
            l[i] = new Line(x1, y1, x2, y2);
        }
        System.out.println(comLen(l));
        System.out.println(l[0].length());
        System.out.println(l[1].sqrRec());
        System.out.println(l[0].equals(l[1]));
        System.out.println(l[2].toString());
        System.out.println(comPoint(l));
    }
}
