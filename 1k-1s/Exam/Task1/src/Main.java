import java.util.Scanner;

public class Main {
    public static boolean check(int[] numbers, int[][] digits) {

        //int tempNum1;
        //int tempNum2;

        for (int i = 0; i < numbers.length - 1; i++) {
            int[] digitNum1 = digitNum(numbers[i]);
            int[] digitNum2 = digitNum(numbers[i + 1]);
            if (!checkInMatrix(digitNum1, digitNum2, digits) && !checkCommon(digitNum1, digitNum2)) {
                return false;
            }
        }
        return true;
    }

    public static int[] digitNum(int x) {
        int digit;
        int[] digitNum1 = new int[10];
        while (x > 0) {
            digit = x % 10;
            digitNum1[digit]++;
            x /= 10;
        }
        return digitNum1;
    }

    public static boolean checkInMatrix(int[] digitNum1, int[] digitNum2, int[][] digits) {
        boolean[] digitNumb1 = new boolean[10];
        boolean[] digitNumb2 = new boolean[10];

        for (int i = 0; i < digits.length; i++) {
            digitNumb1 = digitNumb(digitNum1);
            digitNumb2 = digitNumb(digitNum2);
            for (int j = 0; j < digits[i].length; j++) {
                if (digitNumb1[digits[i][j]])
                    digitNumb1[digits[i][j]] = false;
                if (digitNumb2[digits[i][j]])
                    digitNumb2[digits[i][j]] = false;
            }
            for (int j = 0; j < 10; j++) {
                if (digitNumb1[j] || digitNumb2[j])
                    return false;
            }
            return true;
        }
        for (int i = 0; i < 10; i++) {
            if (digitNum1[i] > 0 && digitNum2[i] > 0)
                return false;
        }
        return false;
    }

    public static boolean[] digitNumb(int[] digitNum) {
        boolean[] digitNumb1 = new boolean[10];
        for (int i = 0; i < 10; i++) {
            if (digitNum[i] > 0)
                digitNumb1[i] = true;
        }
        return digitNumb1;
    }

    public static boolean checkCommon(int[] digitNum1, int[] digitNum2) {
        int i = 0;
        int k = 0;
        while (k < 2 && i < 10) {
            k += Math.min(digitNum1[i], digitNum2[i]);
            i++;
        }
        return k > 1;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt(), //n - размер массива numbers
                m = in.nextInt(), k = in.nextInt();
        // m и k - размерность матрицы digits, m -строк, k - столбцов
        int[] numbers = new int[n];
        int[][] digits = new int[m][k];
        for (int i = 0; i < n; i++) {
            numbers[i] = in.nextInt();
        }
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < k; j++) {
                digits[i][j] = in.nextInt();
            }
        }
        System.out.println(check(numbers, digits));
    }
}
