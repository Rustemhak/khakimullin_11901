import java.util.Scanner;

public class MyString {
    private String s;

    public MyString(String s) {
        this.s = s;
    }

    public MyString() {
        this("");
    }

    public String compare(MyString ms2) {
        String s1 = this.s;
        String s2 = ms2.getS();
        int min = s1.length();
        if (s2.length() < min)
            min = s2.length();
        for (int i = 0; i < min; i++) {
            if (s1.charAt(i) > s2.charAt(i))
                return "Вторая строка раньше";
            else if (s1.charAt(i) < s2.charAt(i))
                return "Первая строка раньше";
        }
        if (s1.length() > min)
            return "Первая строка позже";
        else if (s2.length() > min)
            return "Первая строка раньше";
        else return "Они равны";
    }

    public MyString concat(MyString ms2) {
        return new MyString(this.s + ms2.getS());
    }

    public boolean rejected(MyString ms2) {
        String s1 = this.s;
        String s2 = ms2.getS();
        for (int i = 0; i < s1.length(); i++) {
            for (int j = 0; j < s2.length(); j++) {
                if (s1.charAt(i) == s2.charAt(j))
                    return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return this.s;
    }

    public boolean equals(MyString ms2) {
        String s1 = this.s;
        String s2 = ms2.getS();
        if (s1.length() != s2.length())
            return false;
        else {
            for (int i = 0; i < s1.length(); i++) {
                if (s1.charAt(i) != s2.charAt(i))
                    return false;
            }
            return true;
        }
    }

    public int length() {
        return s.length();
    }

    public char charAt(int i) {
        return s.charAt(i);
    }

    public String getS() {
        return s;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        MyString[] myStrings = new MyString[n];
        for (int i = 0; i < n; i++) {
            myStrings[i] = new MyString(in.next());
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < myStrings[i].length(); j++) {
                System.out.print(myStrings[i].charAt(j));
            }
        }
    }
}



