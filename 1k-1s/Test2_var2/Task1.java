public class Task1{
	public static boolean flag(String[] s){
		int cap, sm;
		for (String i : s){
			cap = 0;
			sm = 0;
			for (int j = 0; j < i.length(); j++){
				if (i.charAt(j) >= 'A' && i.charAt(j) <= 'Z')
						cap++;
				else sm++;
			}
			if (sm <= cap)
				return false;
		}
		return true;
	}
	public static void replace(String[] s, char ins){
		String s1;
		char c;
		for (int i = 0; i < s.length; i++){
				s1 = "";
				
				for (int j = 0; j < s[i].length(); j++){
					c = s[i].charAt(j);
					if (c == 'A' || c == 'e' || c == 'E' ||
					c == 'I' || c == 'i' || c == 'o' || 
					c == 'O' || c == 'u' || c == 'U' ||
					c == 'Y' || c =='y')
					 s1 += ins;
					else s1 += c;
				}
			s[i] = s1;
			}
	}
	public static void main(String [] args){
		int n = Integer.parseInt(args[0]);
		String [] s = new String[n];
		for (int i = 0; i < n; i++)
			s[i] = args[i + 1];
		if (flag(s))
			replace(s,'a');
			for (String i: s)
				System.out.println(i);
	}
}