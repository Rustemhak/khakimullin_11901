import java.util.Scanner;
public class Task01 {
	public static void main(String [] args) {
		Scanner in =  new Scanner(System.in);
		int n = in.nextInt();
		int [] a = new int[n];
		for (int i = 0; i < n; i++)
			a[i] = in.nextInt();
		boolean flag = true;
		boolean ok;
		int digit;
		int i = 0;
	//for (int i = 0; i < n; i++)
		while(i < n && flag){
		ok = false;
		while(a[i] > 0 && !ok){
			digit = a[i] % 10;
			if (digit == 3 || digit == 5)
				ok = true;
			a[i] /= 10;
		}
		if (!ok)
			flag = false;
		i++;
		}
	System.out.println(flag);
	}
}
	