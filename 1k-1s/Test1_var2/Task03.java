//with 2D array
import java.util.Scanner;
public class Task03{
	public static void main(String [] args){
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int[][] a = new int[n][n];
		//int[] a = new int[n];
		int[] time = new int [15];
		for (int i = 0; i < n; i++){
			for (int j = 0; j < n; j++)
				//a[j] = in.nextInt();
				a[i][j]=in.nextInt();
		}
		int hour;
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++){
				//hour = a[j];
				hour = a[i][j];
				time[hour - 7]++;
			}
		
		for (int i = 0; i < 15; i++){
			if (i < 3)
				System.out.print(" ");
			System.out.println((i + 7) + ":00: " + time[i] + " workers");
		}
	}
}