import java.util.Scanner;
public class Block{
	public static void main(String [] args){
		Scanner in= new Scanner(System.in);
		int n=100;
		int [] a = new int[n];
		for(int i=0;i<n;i++)
			a[i]=i;
		int k=40;//длина второго блока
		int last;
		long startTime = System.nanoTime();
		for(int i=0;i<k;i++){
			last=a[n-1];
			for(int j=n-1;j>0;j--)
				a[j]=a[j-1];
			a[0]=last;
		}
		long timeSpent = System.nanoTime();
		System.out.println("программа выполнялась " + (timeSpent-startTime) + " миллисекунд");
		for(int i=0;i<n;i++)
			System.out.print(a[i]+" ");
	}
}
				