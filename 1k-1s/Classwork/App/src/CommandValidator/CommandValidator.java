package CommandValidator;

import Models.Student;
import Models.Teacher;

import java.util.Scanner;

public class CommandValidator {
    public static void main(String[] args) throws Exception {
        Scanner in = new Scanner(System.in);
        while (in.hasNextLine()) {
            String[] s = in.nextLine().split(" ");
            if (s[0].equals("new") && s[1].equals("student")) {
                Student student = new Student();
                for (String i : s) {
                    if (!i.contains("=")) {
                        String[] param = i.split("=");
                        if (s[0] == "id") {
                            student.setId(Integer.parseInt(s[1]));
                        }
                        else if(s[0] == "name"){
                            student.setName(s[1]);
                        }
                    } else throw new RuntimeException("wrong command");
                }
            }
            else if (s[0].equals("new") && s[1].equals("teacher")) {
                Teacher teacher = new Teacher();
                for (String i : s) {
                    if (!i.contains("=")) {
                        String[] param = i.split("=");
                        if (param[0] == "id") {
                            teacher.setId(Integer.parseInt(s[1]));
                        }
                    } else throw new Exception("wrong command");
                }
            }
        }
    }
}
