package Models;

import Genders.Gender;

import java.util.ArrayList;
import java.util.Calendar;

public class Student {
    private String name, middleName, surname;
    private String hometown;
    private Calendar birthday;

    private String group, institute;
    private int IQ;
    private int id;
    private Gender gender;
    private String username;
    private ArrayList<Interest> skills = new ArrayList<>();
    private ArrayList<Skill> interests = new ArrayList<>();

    public Student() {

    }

    public Student(int id, String name, String surname, String middleName, Calendar birthday,
                   String group, String institute, Gender gender, int IQ,
                   ArrayList<Interest> interests,
                   ArrayList<Skill> skills) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.middleName = middleName;
        this.birthday = birthday;
        this.group = group;
        this.institute = institute;
        this.gender = gender;
        this.IQ = IQ;
    }

    public String getHometown() {
        return hometown;
    }

    public void setHometown(String hometown) {
        this.hometown = hometown;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Calendar getBirthday() {
        return birthday;
    }

    public void setBirthday(Calendar birthday) {
        this.birthday = birthday;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getInstitute() {
        return institute;
    }

    public void setInstitute(String institute) {
        this.institute = institute;
    }

    public int getIQ() {
        return IQ;
    }

    public void setIQ(int IQ) {
        this.IQ = IQ;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Interest> getSkills() {
        return skills;
    }

    public void setSkills(ArrayList<Interest> skills) {
        this.skills = skills;
    }

    public ArrayList<Skill> getInterests() {
        return interests;
    }

    public void setInterests(ArrayList<Skill> interests) {
        this.interests = interests;
    }

    @Override
    public String toString() {
        return this.name + " " + this.middleName + " " + this.surname + " " +
                this.hometown + " " +
                this.birthday + " " +
                this.group + " " + this.institute + " " +
                this.IQ + " " +
                this.id + " " +
                this.gender + " " + this.username;
    }
}
