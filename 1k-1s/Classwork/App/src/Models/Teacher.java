package Models;

import Genders.Gender;
import com.sun.org.apache.xpath.internal.operations.String;

import java.util.ArrayList;
import java.util.Calendar;

public class Teacher {

        private int id;
        private String name,surname, middleName;
        private String hometown;
        private Calendar birthday;
        private String institute;
        private String position;
        private Gender gender;
        private int exp;
        private String username;
        private ArrayList<Subject> subjects;
        private ArrayList<Interest> interests;
        private ArrayList<Skill> skills;
        public Teacher() {
        }
        public Teacher(int id, String name, String surname, String middleName, String hometown, Calendar birthday,
                       String institute, String position, Gender gender, int exp, String username,
                       ArrayList<Subject>subjects, ArrayList<Interest> interests, ArrayList<Skill> skills){
            this.id = id;
            this.name = name;
            this.surname = surname;
            this.middleName = middleName;
            this.hometown = hometown;
            this.birthday  = birthday;
            this.institute = institute;
            this.position = position;
            this.gender = gender;
            this.exp = exp;
            this.username = username;
        }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getHometown() {
        return hometown;
    }

    public void setHometown(String hometown) {
        this.hometown = hometown;
    }

    public Calendar getBirthday() {
        return birthday;
    }

    public void setBirthday(Calendar birthday) {
        this.birthday = birthday;
    }

    public String getInstitute() {
        return institute;
    }

    public void setInstitute(String institute) {
        this.institute = institute;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    public ArrayList<Interest> getInterests() {
        return interests;
    }

    public void setInterests(ArrayList<Interest> interests) {
        this.interests = interests;
    }

    public ArrayList<Skill> getSkills() {
        return skills;
    }

    public void setSkills(ArrayList<Skill> skills) {
        this.skills = skills;
    }

    public ArrayList<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(ArrayList<Subject> subjects) {
        this.subjects = subjects;
    }
}
