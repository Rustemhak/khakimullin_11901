import java.util.regex.*;
public class RegexExample {
	
	public static void main(String [] args) {
		
		Pattern p = Pattern.compile("[1-9][0-9]+");
		Matcher m = p.matcher("http://vk.com/id1234123" + 
								"http://vk.com/id342432432");
		System.out.println(m.matches());
		while (m.find())
			System.out.println(m.group());
		m.reset();
		while (m.find())
			System.out.println(m.group());
	}
}