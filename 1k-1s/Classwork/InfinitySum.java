
public class InfinitySum{
	public static void main(String [] args){
		double x=Double.parseDouble(args[0]);
		final double EPS=1e-9;//0.000000001
		int k=1;
		double sum=1;
		double deg=1;
		int fact=1;
		double a=deg/fact;
		while(Math.abs(a)>EPS){
			deg*=x;
			fact*=k;
			a=deg/fact;
			sum+=a;
			k++;
		}
		System.out.println(sum);
	}
}