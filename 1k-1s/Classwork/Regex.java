import java.util.regex.*;
public class Regex{
	public static void main(String [] args){
		//целое
		Pattern pz = Pattern.compile("0|-?[1-9][0-9]*");
		Matcher mz = pz.matcher(args[0]);
		System.out.println(mz.matches());
		Matcher mz1 = pz.matcher(args[0]);
		while(mz1.find())
			System.out.println(mz1.group());
		//Переменная - id
		Pattern pid = Pattern.compile("[a-zA-z_][A-Za-z0-9_]*");
		Matcher mid = pid.matcher(args[1]);
		System.out.println(mid.matches());
		Matcher mid1 = pid.matcher(args[1]);
		while(mid1.find())
			System.out.println(mid1.group());
		//number of phone
		Pattern pnum = Pattern.compile("\\+7\\([0-9]{3}\\)[0-9]{3}-[0-9]{2}-[0-9]{2}");
		Matcher mnum = pnum.matcher(args[2]);
		System.out.println(mnum.matches());
		Matcher mnum1 = pnum.matcher(args[2]);
		while(mnum1.find())
			System.out.println(mnum1.group());
		//time
		Pattern ptime = Pattern.compile("([01][0-9]|2[0-3]):([0-5][0-9])");
		Matcher mtime = ptime.matcher(args[0]);
		System.out.println(mtime.matches());
		Matcher mtime1 = ptime.matcher(args[0]);
		while(mtime1.find())
			System.out.println(mtime1.group());
		//years of Pushkin
		Pattern pyp = Pattern.compile("1799|18([0-2][0-9]|3[0-7])");
		Matcher myp = pyp.matcher(args[1]);
		System.out.println(myp.matches());
		Matcher myp1 = pyp.matcher(args[1]);
		while(myp1.find())
			System.out.println(myp1.group());
		//Real
		Pattern pr = Pattern.compile("0|-?[1-9][0-9]*|-?[0-9]+,[0-9]*[1-9]+[0-9]*|-?[0-9]+,[0-9]*\\([0-9]*[1-9][0-9]*\\)");
		Matcher mr = pr.matcher(args[2]);
		System.out.println(mr.matches());
		Matcher mr1 = pr.matcher(args[2]);
		while(mr1.find())
			System.out.println(mr1.group());
	}
}