public class ProdSum{
	public static void main(String [] args){
		int n=Integer.parseInt(args[0]);
		int m=Integer.parseInt(args[1]);
		double deg=(1<<m);
		//System.out.println(deg);
		double fact=1;
		for(int i=2;i<=m;i++)
			fact*=i;
		//System.out.println(fact);
			double prod=1;
		for(int i=1;i<=n;i++)
			prod*=m*(deg/fact+i);
		System.out.println(prod);
		}
	}
	