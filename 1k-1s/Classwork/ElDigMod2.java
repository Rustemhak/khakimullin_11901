import java.util.Scanner;

public class ElDigMod2 {
	public static void main(String [] args) {
		Scanner in =  new Scanner(System.in);
		int n = in.nextInt();
		int [] a = new int[n];
		for (int i = 0; i < n; i++)
			a[i] = in.nextInt();
		boolean ans = false;
		boolean b;
		for (int i = 0; i < n; i++) {
			b = true;
			while (a[i] !=0) {
			if ((a[i] % 10) % 2 != 0)
				b = false;
			
			a[i] /= 10;
			}
			if (b)
				ans = true;
		}
		System.out.println(ans ? "Yes" : "No");
	}
}