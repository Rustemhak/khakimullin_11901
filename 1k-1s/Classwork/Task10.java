/**
* @author Rustem Khakimullin
* 11-901
* Task 10
*/

public class Task10 {
	public static void main(String args[]) {
		double x = Double.parseDouble(args[0]);
		double y = Double.parseDouble(args[1]);
		double y1 = ( - 2.0) * x + 4.0;
		boolean flag = false;
		if ( y >= 0.0 && x <= 0.0 && y * y + x * x <= 16.0 )
			flag = true;
		else if(y1 >= 0.0 && x >= 0.0 && x <= 2.0 && y1 <= 4.0 && y <= y1)
			flag = true;
		System.out.println(flag ? "Yes" : "No");
	}
}