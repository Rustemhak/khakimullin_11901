public class Task01 	{
	public static void square(int m, int n){
		for(int i = 0; i < m; i++){
			drawLine("0", n);
			newLine();
		}
	}
	
	public static void newLine(){
		System.out.println();
	}
	
	public static void drawLine(String s, int n){
		for (int i = 1; i <= n; i++)
			System.out.print(s);
	}
	
	
	public static void drawRightTriangle(String s, int n){
		for (int i =1;i <= n ;i++){
			drawLine("*", i);
			newLine();
		}
	}
	
	
	public static void par(int m, int n){
		for(int i = 0; i < m; i++){
			drawLine(" ", i)
			drawLine("a", n);
			newLine();
		}
	}
	
	public static void main(String [] args) {
		int n = 10;
		int m = 5;
		for (int i =1;i<=n;i++){
			drawLine("*", i);
			newLine();
		}
		square(m, n);
	}
}