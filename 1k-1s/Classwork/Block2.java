import java.util.Scanner;
public class Block2{
	public static void main(String [] args){
		Scanner in= new Scanner(System.in);
		int n=in.nextInt();
		int k=in.nextInt();
		int [] a=new int[n];
		for(int i=0;i<n;i++)
			a[i]=in.nextInt();
		int swap;
		for(int i=n-1;i>=k;i--){
			if(i>=n-k)
				a[i-(n-k)]=a[i];
		
			a[i]=a[i-k];
		}
		for(int i:a)
			System.out.print(i+" ");
	}
}