public class Task4{
	public static void main(String args []){
		int radius=Integer.parseInt(args[0]);
		for(int i=0;i<=radius*2;i++){
			for(int j=0;j<=radius*2;j++){
				if((radius-i)*(radius-i)+(radius-j)*(radius-j)<=radius*radius)
					System.out.print("1");
				else System.out.print(" ");
			}
			System.out.println();
		}
	}
}