/**
* @author Rustem Khakimullin
* 11-901
* Task 26
*/
import java.util.regex.*;
 public class Task26{
	 public static void main(String [] args){
		Pattern pr = Pattern.compile("0|-?[1-9][0-9]*|-?[0-9]+,[0-9]*[1-9]+[0-9]*|-?[0-9]+,[0-9]*\\([0-9]*[1-9][0-9]*\\)");
		Matcher mr = pr.matcher(args[0]);
		System.out.println(mr.matches());
		Matcher mr1 = pr.matcher(args[0]);
		while(mr1.find())
			System.out.println(mr1.group());
	 }
 }