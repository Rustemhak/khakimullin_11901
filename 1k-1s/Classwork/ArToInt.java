import java.util.Scanner;
public class ArToInt{
	public static void main(String [] args){
		Scanner in= new Scanner(System.in);
		int n=in.nextInt();
		int [] a=new int[n];
		for(int i=0;i<n;i++)
			a[i]=in.nextInt();
		int ans=0;
		int deg10=1;
		for(int i=n-1;i>=0;i--){
			ans+=a[i]*deg10;
			while(a[i]!=0){
			 deg10*=10;
			 a[i]/=10;
			}
		}
		System.out.println(ans);
	}
}