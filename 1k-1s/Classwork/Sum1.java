public class Sum1{
	public static void main(String [] args){
		double x=Double.parseDouble(args[0]);
		final double EPS=1e-9;
		
		double deg=x;
		int k=1;
		int sign=1;
		k++;
		double a=sign*deg/k;
		double sum=a;
		double sumbef=0;
		while(Math.abs(a)>EPS){
			sign*=-1;
			deg*=x;
			a=sign*deg/k;
			sumbef=sum;
			sum+=a;
			k++;
			System.out.println(sum);
		}
		System.out.println(sum);
	}
}