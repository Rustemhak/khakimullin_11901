public class Strings{
	public static boolean equals(String s1, String s2){
		if (s1.length() != s2.length())
			return false;
		else {
			for(int i = 0; i < s1.length(); i++){
				if (s1.charAt(i) != s2.charAt(i))
					return false;
			}
			return true;
		}
	}
	public static boolean existsB(String s1, String s2){
		String [] word = s1.split(" ");
		for (String elem : word)
			if (equals(elem,s2))
				return true;
		return false;
	}
	public static boolean existsA(String s, String s2){
		String subs;
		int i = 0;
		while(i < s.length()){
			subs = "";
			while (i < s.length() && s.charAt(i) != ' ' ){
				subs += s.charAt(i);
				i++;
			}
			if (equals(subs,s2))
				return true;
			i++;
		}
		return false;
	}
	public static int compare(String s1, String s2){
		int min = s1.length();
		if (s2.length() < s1.length())
			min = s2.length();
		for (int i = 0; i < min; i++){
			if (s1.charAt(i) > s2.charAt(i))
				return 1;
			else if (s1.charAt(i) < s2.charAt(i))
				return -1;
		}
		if (s1.length() > min)
			return 1;
		else if (s2.length() > min)
			return -1;
		else return 0;
	}
	public static void main(String [] args){
		String s1 = "Mum was washing window";
		String s2 = args[0];
		System.out.println(equals(s1,s2));
		System.out.println(compare(s1,s2));
		System.out.println(existsA(s1,s2));
		System.out.println(existsB(s1,s2));
	}
}