import java.util.Scanner;
public class Turn{
	public static void main(String [] args){
		Scanner in= new Scanner(System.in);
		int n=in.nextInt();
		int [] a= new int[n];
		for(int i=0;i<n;i++)
		 a[i]=in.nextInt();
		int swap;
		for(int i=0;i<n/2;i++){
			swap=a[i];
			a[i]=a[n-i-1];
			a[n-i-1]=swap;
		}
		for(int i=0;i<n;i++)
			System.out.print(a[i]+" ");
	}
}