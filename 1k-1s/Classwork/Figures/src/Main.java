import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("input.txt"));
        Figures[] f =new Figures[1000];
        int i = 0;
        while (sc.hasNext()){
            String a = sc.next();
            if (a.equals("rect")) {

                f[i]=new Rect(sc.nextDouble(),sc.nextDouble());
            }
            else if(a.equals("tri")){
                f[i]= new Tri(sc.nextDouble(),sc.nextDouble(),sc.nextDouble());

            }
            else if(a.equals("circ")){
                f[i] = new Circ(sc.nextDouble());
            }
            i++;

        }
        for (Figures figures: f) {
            if(figures !=null){
                System.out.println(figures.p());
                System.out.println(figures.s());
            }

        }
    }
}
