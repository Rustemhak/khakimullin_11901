import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Random;
import java.util.Scanner;

public class Randomizer {
    public static void main(String[] args) throws FileNotFoundException {

        PrintWriter out = new PrintWriter(new File("input.txt"));
        Random random = new Random();
        for (int i = 0; i < Math.abs(random.nextInt() % 1000); i++) {
            int a = Math.abs(random.nextInt() % 3);
            if (a == 0) {
                out.println("rect " + Math.abs(random.nextDouble()*1000) + " " + Math.abs(random.nextDouble()*1000));
            }
            ;
            if (a == 1) {
                out.println("tri " + Math.abs(random.nextDouble()*1000) + " "
                        + Math.abs(random.nextDouble()*1000) + " " + Math.abs(random.nextDouble()*1000));
            }
            if (a == 2) {
                out.println("circ " + Math.abs(random.nextDouble()*1000));
            }
        }
        out.close();

    }
}
