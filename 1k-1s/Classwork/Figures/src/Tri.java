public class Tri extends Figures {
    private double a, b, c;

    public Tri(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }
    @Override
    public double p() {
        return this.a + this.b + this.c;
    }
    @Override
    public double s() {
        return Math.sqrt(p() / 2 * (p() / 2 - this.a) * (p() / 2 - b) * (p() / 2 - c));
    }
}
