public class Rect extends Figures {
    private double a,b;
    public Rect(double a, double b){
        this.a = a;
        this.b = b;
    }
    @Override
    public  double s(){
        return this.a * this.b;
    }
    @Override
    public double p(){
        return (this.a + this.b) * 2;
    }
}
