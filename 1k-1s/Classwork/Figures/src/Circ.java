public class Circ extends Figures {
    private double r;

    public Circ(double r) {
        this.r = r;
    }
    @Override
    public double s() {
        return Math.PI * r * r;
    }

    @Override
    public double p() {
        return Math.PI * r * 2;
    }
}
