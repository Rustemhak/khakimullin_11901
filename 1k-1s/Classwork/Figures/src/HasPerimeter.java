public interface HasPerimeter {
    double p();
}
