import java.util.Scanner;

public class ElMod2 {
	public static void main(String [] args) {
		Scanner in =  new Scanner(System.in);
		int n = in.nextInt();
		int [] a = new int[n];
		for (int i = 0; i < n; i++)
			a[i] = in.nextInt();
		boolean b = false;
		for (int i = 0; i < n && b; i++){
			if (a[i] % 2 == 0)
				b = true;
			
		}
		System.out.println(b ? "Yes" : "No");
	}
}