public class Sum3{
	public static void main(String [] args){
		int m=Integer.parseInt(args[0]);
		int n=Integer.parseInt(args[1]);
		if(m>n){
			int swap=m;
			m=n;
			n=swap;
		}
		long fact1=1;
		long sum=0;
		for(int i=1;i<=m;i++){
			fact1*=i;
			sum+=fact1;
		}
		sum*=(m+n);
		long s2=0;
		for(int i=m+1;i<=n;i++){
			fact1*=i;
			s2+=fact1;
		}
		s2*=m;
		sum+=s2;
		System.out.println(sum);
	}
}