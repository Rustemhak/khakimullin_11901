/**
* @author Rustem Khakimullin
* 11-901
* Task 08
*/

public class Task08 {
	public static void main(String args []) {
		int radius = Integer.parseInt(args[0]);
		for(int i = 0; i <= radius * 2; i++){
			for(int j = 0; j <= radius * 2; j++){
				if((radius - i) * (radius - i) + (radius - j) * (radius - j) <= radius * radius)
					System.out.print("0");
				else System.out.print("*");
			}
			System.out.println();
		}
	}
}