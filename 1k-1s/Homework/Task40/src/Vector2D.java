/**
 * @author Rustem Khakimullin
 * 11-901
 * for Task 40
 */

public class Vector2D {
    private double x, y;

    //нуль-вектор
    public Vector2D() {
        this(0, 0);
    }

    public Vector2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public Vector2D add(Vector2D v) {
        return new Vector2D(
                this.x + v.getX(),
                this.y + v.getY()
        );
    }

    public void add2(Vector2D v) {
        this.x += v.getX();
        this.y += v.getY();
    }

    public Vector2D sub(Vector2D v) {
        return new Vector2D(
                this.x - v.getX(),
                this.y - v.getY()
        );
    }

    public void sub2(Vector2D v) {
        this.x -= v.getX();
        this.y -= v.getY();
    }

    public Vector2D mult(double a) {
        return new Vector2D(
                this.x * a,
                this.y * a
        );
    }

    public void mult2(double a) {
        this.setX(this.getX() * a);
        this.setY(this.getY() * a);
    }

    @Override
    public String toString() {
        return "(" + this.getX() + ", " + this.getY() + ")";
    }

    public double length() {
        return Math.sqrt(this.getX() * this.getX() + this.getY() * this.getY());
    }


    public double scalarProduct(Vector2D v) {
        return this.getX() * v.getX() + this.getY() * v.getY();
    }

    public double cos(Vector2D v) {
        return this.scalarProduct(v) / (this.length() * v.length());
    }

    public boolean equals(Vector2D v1) {
        if (v1.getX() == this.getX() && v1.getY() == this.getY())
            return true;
        else return false;
    }
}
