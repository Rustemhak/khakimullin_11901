/**
 * @author Rustem Khakimullin
 * 11-901
 * for Task 40
 */

public class DetIsZeroException extends Exception {
    public DetIsZeroException(String message){
        super(message);
    }
}
