/**
 * @author Rustem Khakimullin
 * 11-901
 * Task 40
 */

public class Matrix2x2 {
    private double x, x1, x2, x3, x4;
    private double[][] a = new double[2][2];


    public Matrix2x2() {
        this(0);
    }

    public Matrix2x2(double x) {
        this.a[0][0] = x;
        this.a[0][1] = x;
        this.a[1][0] = x;
        this.a[1][1] = x;
    }

    public Matrix2x2(double[][] a) {
        this(a[0][0], a[0][1], a[1][0], a[1][1]);
    }

    public Matrix2x2(double x1, double x2, double x3, double x4) {
        this.a[0][0] = x1;
        this.a[0][1] = x2;
        this.a[1][0] = x3;
        this.a[1][1] = x4;
    }

    public double[][] getA() {
        return a;
    }

    public void setA(double[][] a) {
        this.a = a;
    }

    public Matrix2x2 add(Matrix2x2 m) {
        double[][] an = new double[2][2];
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                an[i][j] = this.a[i][j] + m.getA()[i][j];
            }
        }
        return new Matrix2x2(an);
    }

    public void add2(Matrix2x2 m) {
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                this.a[i][j] += m.getA()[i][j];
            }
        }
    }

    public Matrix2x2 sub(Matrix2x2 m) {
        double[][] an = new double[2][2];
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                an[i][j] = this.a[i][j] - m.getA()[i][j];
            }
        }
        return new Matrix2x2(an);
    }

    public void sub2(Matrix2x2 m) {
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                this.a[i][j] -= m.getA()[i][j];
            }
        }
    }

    public Matrix2x2 multNumber(double k) {
        double[][] an = new double[2][2];
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                an[i][j] = this.a[i][j] * k;
            }
        }
        return new Matrix2x2(an);
    }

    public void multNumber2(double k) {
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                this.a[i][j] *= k;
            }
        }
    }

    public Matrix2x2 mult(Matrix2x2 m) {
        double[][] an = new double[2][2];
        for (int i = 0; i < 2; i++)
            for (int j = 0; j < 2; j++)
                for (int k = 0; k < 2; k++)
                    an[i][j] += this.a[i][k] * m.getA()[k][j];
        return new Matrix2x2(an);
    }

    public void mult2(Matrix2x2 m) {
        double[][] an = new double[2][2];
        for (int i = 0; i < 2; i++)
            for (int j = 0; j < 2; j++)
                for (int k = 0; k < 2; k++)
                    an[i][j] += this.a[i][k] * m.getA()[k][j];
        for (int i = 0; i < 2; i++)
            for (int j = 0; j < 2; j++)
                this.a[i][j] = an[i][j];
    }

    public double det() {
        return this.a[0][0] * this.a[1][1] - this.a[1][0] * this.a[0][1];
    }

    public void transpon() {
        double t = this.a[0][1];
        this.a[0][1] = this.a[1][0];
        this.a[1][0] = t;
    }

    public Matrix2x2 inverseMatrix() throws DetIsZeroException {
        double d = this.det();
        if (d != 0) {
            Matrix2x2 m = new Matrix2x2(this.a[1][1], -this.a[1][0], -this.a[0][1], this.a[0][0]);
            m.transpon();
            double[][] ar = new double[2][2];
            for (int i = 0; i < 2; i++)
                for (int j = 0; j < 2; j++)
                    ar[i][j] = m.getA()[i][j] / d;
            m.setA(ar);
            return m;
        } else {
            throw new DetIsZeroException("Matrix don't have inverse Matrix");
        }
    }


    public Matrix2x2 equivalentDiagonal() {
        double[][] ar = new double[2][2];
        for (int i = 0; i < 2; i++)
            for (int j = 0; j < 2; j++)
                ar[i][j] = this.a[i][j];
        if (ar[0][0] == 0) {
            double t1 = ar[1][0];
            ar[1][0] = ar[0][0];
            ar[0][0] = t1;
            double t2 = ar[1][1];
            ar[1][1] = ar[0][1];
            ar[0][1] = t2;
        }
        if (ar[0][0] != 0) {
            for (int j = 0; j < 2; j++) {
                ar[1][j] -= ar[0][j] * ar[1][j] / ar[0][0];
            }
            if (ar[1][1] != 0)
                ar[0][1] = 0.0;
        }
        Matrix2x2 m = new Matrix2x2(ar[0][0], ar[0][1], ar[1][0], ar[1][1]);
        return m;
    }

    public Vector2D multVector(Vector2D v) {
        return new Vector2D(this.a[0][0] * v.getX() + this.a[0][1] * v.getY(),
                this.a[1][0] * v.getX() + this.a[1][1] * v.getY());
    }

    @Override
    public String toString() {
        return getA()[0][0] + " " + getA()[0][1] + "\n" + getA()[1][0] + " " + getA()[1][1];
    }
}
