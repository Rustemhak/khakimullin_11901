/**
* @author Rustem Khakimullin
* 11-901
* Task 14 second solution
*/

public class Task14B 	{
	public static void main(String [] args) {
		double x = Double.parseDouble(args[0]);
		double y = ( x > 2 ? (x * x - 1) / (x + 2) :  x > 0 && x <= 2  ? (x * x - 1) * (x + 2) : x * x * (1 + 2 * x));
		System.out.println(y);
	}
}
