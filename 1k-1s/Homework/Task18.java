/**
* @author Rustem Khakimullin
* 11-901
* Task18
*/


public class Task18{
		public static void main(String []  args){
		double x = Double.parseDouble(args[0]);
		final double EPS = 1e-9;
		int sign = -1;
		double deg = 1;
		int fact = 1;
		double a = -1;
		double sum = a;
		int i = 2;	//i = 2k - 2
		while(Math.abs(a) > EPS){
			sign *= - 1;
			deg *= (x * x);
			fact *= (i * (i - 1));
			a = (sign * deg) / fact;
			sum += a ;
			i += 2;
			System.out.println(sum);
		}
		System.out.println(sum);
	}
}