/**
* @author Rustem Khakimullin
* 11-901
* Task 26
*/
import java.util.regex.*;
import java.util.Scanner;
 public class Task26{
	 public static void main(String [] args){
		Scanner in = new Scanner(System.in);
		String s = in.nextLine();
		Pattern pr = Pattern.compile("[-+]?[0-9]+[.,][0-9]*\\([0-9]*[1-9][0-9]*\\)|[-+]?0[.,][0-9]*[1-9]+|[-+]?[0-9]*[1-9][.,][0-9]*[1-9]+|[-+]?[1-9][0-9]*|0");
		Matcher mr = pr.matcher(s);
		System.out.println(mr.matches());
	 }
 }