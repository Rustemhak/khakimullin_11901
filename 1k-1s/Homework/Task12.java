/**
* @author Rustem Khakimullin
* 11-901
* Task 12
*/

public class Task12 {
	public static void main(String [] args) {
		int n = Integer.parseInt(args[0]);
		boolean flag = false;
		while(n != 0 && !flag){
			if (n % 10 == 5)
				flag = true;
			n /= 10;
		}
		System.out.println(flag ? "Yes" : "No");
	}
}