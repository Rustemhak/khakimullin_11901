/**
 * @author Rustem Khakimullin
 * 11-901
 * Task 46
 */
public class RationalComplexVector2D {
    private RationalComplexNumber x, y;

    public RationalComplexVector2D() {
        RationalComplexNumber rc = new RationalComplexNumber();
        this.x = rc;
        this.y = rc;
    }

    public RationalComplexVector2D(RationalComplexNumber x, RationalComplexNumber y) {
        this.x = x;
        this.y = y;
    }

    public RationalComplexNumber getX() {
        return x;
    }

    public RationalComplexNumber getY() {
        return y;
    }

    public RationalComplexVector2D add(RationalComplexVector2D rcv) {
        return new RationalComplexVector2D(this.x.add(rcv.getX()), this.y.add(rcv.getY()));
    }

    @Override
    public String toString() {
        return "(" + this.x.toString() + ";" + this.y.toString() + ")";
    }

    public RationalComplexNumber scalarProduct(RationalComplexVector2D rcv) {
        return this.x.mult(rcv.getX()).add(this.y.mult(rcv.getY()));
    }
}
