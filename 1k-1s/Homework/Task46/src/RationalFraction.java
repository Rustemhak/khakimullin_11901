/**
 * @author Rustem Khakimullin
 * 11-901
 * for Task 46
 */

public class RationalFraction {
    private int numerator, denominator;
    public RationalFraction() {
        this(0, 1);
    }

    public RationalFraction(int numerator, int denominator) {
        this.numerator = numerator;
        this.denominator = denominator;
    }

    int gcd(int x, int y) {
        x = Math.abs(x);
        y = Math.abs(y);
        while (x > 0 && y > 0) {
            if (x > y)
                x %= y;
            else y %= x;
        }
        return x + y;
    }

    int lcm(int x, int y) {
        x = Math.abs(x);
        y = Math.abs(y);
        return x / gcd(x, y) * y;
    }

    public void reduce() {
        int div = gcd(this.numerator, this.denominator);
        this.numerator /= div;
        this.denominator /= div;
    }

    public int getNumerator() {
        return numerator;
    }

    public int getDenominator() {
        return denominator;
    }


    public RationalFraction add(RationalFraction rf) {
        int cd = lcm(this.denominator, rf.getDenominator());
        RationalFraction rf1 = new RationalFraction(
                this.numerator * cd / this.denominator + rf.getNumerator() * cd / rf.getDenominator(),
                cd
        );
        rf1.reduce();
        return rf1;
    }

    public void add2(RationalFraction rf) {
        int cd = lcm(this.denominator, rf.getDenominator());
        this.numerator = this.numerator * cd / this.denominator + rf.getNumerator() * cd / rf.getDenominator();
        this.denominator = cd;
        this.reduce();
    }

    public RationalFraction sub(RationalFraction rf) {
        int cd = lcm(this.denominator, rf.getDenominator());
        RationalFraction rf1 = new RationalFraction(
                this.numerator * cd / this.denominator - rf.getNumerator() * cd / rf.getDenominator(),
                cd
        );
        rf1.reduce();
        return rf1;
    }

    public void sub2(RationalFraction rf) {
        int cd = lcm(this.denominator, rf.getDenominator());
        this.numerator = this.numerator * cd / this.denominator + rf.getNumerator() * cd / rf.getDenominator();
        this.denominator = cd;
        this.reduce();
    }

    public RationalFraction mult(RationalFraction rf) {
        RationalFraction rf1 = new RationalFraction(
                this.numerator * rf.getNumerator(),
                this.denominator * rf.getDenominator()
        );
        rf1.reduce();
        return rf1;
    }

    public void mult2(RationalFraction rf) {
        this.numerator *= rf.getNumerator();
        this.denominator *= rf.getDenominator();
        this.reduce();
    }

    public RationalFraction div(RationalFraction rf) {
        RationalFraction rf1 = new RationalFraction(
                this.numerator * rf.getDenominator(),
                this.numerator * rf.getNumerator()
        );
        rf1.reduce();
        return rf1;
    }

    public void div2(RationalFraction rf) {
        this.numerator *= rf.getDenominator();
        this.denominator *= rf.getNumerator();
        this.reduce();
    }


    @Override
    public String toString() {
        return this.numerator + "/" + this.denominator;
    }

    public double value() {
        return (double) this.numerator / this.denominator;
    }

    public boolean equals(RationalFraction rf) {
        this.reduce();
        rf.reduce();
        if (this.numerator == rf.getNumerator() && this.denominator == rf.getDenominator())
            return true;
        else return false;
    }

    public int numberPart() {
        return this.numerator / this.denominator;
    }
}
