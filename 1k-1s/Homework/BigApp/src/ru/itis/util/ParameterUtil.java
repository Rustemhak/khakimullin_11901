package ru.itis.util;

import ru.itis.model.Gender;
import ru.itis.model.Student;
import ru.itis.model.Teacher;

public class ParameterUtil {

    public static void applyParameter(Student student, Parameter parameter) {
        String rawValue = parameter.getValue();
        switch (parameter.getName()) {
            case "id":
                try {
                    student.setId(Integer.parseInt(rawValue));
                } catch (Exception e) {
                    System.out.println("Warning: wrong id - " + rawValue + " - skipped");
                }
                return;
            case "fio":
                student.setFio(rawValue);
                return;
            case "city":
                student.setCity(rawValue);
                return;
            case "birthday":
                student.setBirthday(rawValue);
                return;
            case "group":
                student.setGroup(rawValue);
                return;
            case "institute":
                student.setInstitute(rawValue);
                return;
            case "gender":
                if (rawValue.equals("male")) {
                    student.setGender(Gender.MALE);
                } else if (rawValue.equals("female")) {
                    student.setGender(Gender.FEMALE);
                } else {
                    System.out.println("Warning: unknown gender - " + rawValue + " - skipped");
                }
            case "username":
                student.setUsername(rawValue);
                return;
            case "iq":
                try {
                    student.setIq(Integer.parseInt(rawValue));
                } catch (Exception e) {
                    System.out.println("Warning: wrong iq - " + rawValue + " - skipped");
                }
                return;
            default:
                System.out.println("Warning: unknown parameter name - " + parameter.getName() + " - skipped");
        }
    }

    public static void applyParameter(Teacher teacher, Parameter parameter) {
        String rawValue = parameter.getValue();
        switch (parameter.getName()) {
            case "id":
                try {
                    teacher.setId(Integer.parseInt(rawValue));
                } catch (Exception e) {
                    System.out.println("Warning: wrong id - " + rawValue + " - skipped");
                }
                return;
            case "fio":
                teacher.setFio(rawValue);
                return;
            case "city":
                teacher.setCity(rawValue);
                return;
            case "birthday":
                teacher.setBirthday(rawValue);
                return;
            case "institute":
                teacher.setInstitute(rawValue);
                return;
            case "gender":
                if (rawValue.equals("male")) {
                    teacher.setGender(Gender.MALE);
                } else if (rawValue.equals("female")) {
                    teacher.setGender(Gender.FEMALE);
                } else {
                    System.out.println("Warning: unknown gender - " + rawValue + " - skipped");
                }
            case "username":
                teacher.setUsername(rawValue);
                return;
            case "position":
                teacher.setPosition(rawValue);
                return;
            case "experience":
                try {
                    teacher.setExperience(Integer.parseInt(rawValue));
                } catch (Exception e) {
                    System.out.println("Warning: wrong experience (must be in years) - " + rawValue + " - skipped");
                }
                return;
            default:
                System.out.println("Warning: unknown parameter name - " + parameter.getName() + " - skipped");
        }
    }

    public static boolean matchParameter(Teacher teacher, Parameter parameter) {
        String rawValue = parameter.getValue();
        switch (parameter.getName()) {
            case "id":
                try {
                    return teacher.getId() == Integer.parseInt(rawValue);
                } catch (Exception e) {
                    System.out.println("Warning: wrong id - " + rawValue + " - skipped");
                }
            case "fio":
                return teacher.getFio().equals(rawValue);
            case "birthday":
                return teacher.getBirthday().equals(rawValue);
            case "institute":
                return teacher.getInstitute().equals(rawValue);
            case "city":
                return teacher.getCity().equals(rawValue);
            case "gender":
                if (rawValue.equals("male")) {
                    return teacher.getGender().equals(Gender.MALE);
                } else if (rawValue.equals("female")) {
                    return teacher.getGender().equals(Gender.FEMALE);
                } else {
                    System.out.println("Warning: unknown gender - " + rawValue + " - skipped");
                }
            case "username":
                return teacher.getUsername().equals(rawValue);
            case "position":
                return teacher.getPosition().equals(rawValue);
            case "experience":
                try {
                    return teacher.getExperience() == Integer.parseInt(rawValue);
                } catch (Exception e) {
                    System.out.println("Warning: wrong experience (must be in years) - " + rawValue + " - skipped");
                }
            default:
                System.out.println("Warning: unknown parameter name - " + parameter.getName() + " - skipped");
                return false;
        }
    }

    public static boolean matchParameter(Student student, Parameter parameter) {
        String rawValue = parameter.getValue();
        switch (parameter.getName()) {
            case "id":
                try {
                    return student.getId() == Integer.parseInt(rawValue);
                } catch (Exception e) {
                    System.out.println("Warning: wrong id - " + rawValue + " - skipped");
                }
            case "fio":
                return student.getFio().equals(rawValue);
            case "birthday":
                return student.getBirthday().equals(rawValue);
            case "city":
                return student.getCity().equals(rawValue);
            case "institute":
                return student.getInstitute().equals(rawValue);
            case "gender":
                if (rawValue.equals("male")) {
                    return student.getGender().equals(Gender.MALE);
                } else if (rawValue.equals("female")) {
                    return student.getGender().equals(Gender.FEMALE);
                } else {
                    System.out.println("Warning: unknown gender - " + rawValue + " - skipped");
                }
            case "username":
                return student.getUsername().equals(rawValue);
            case "group":
                return student.getGroup().equals(rawValue);
            case "iq":
                try {
                    return student.getIq() == Integer.parseInt(rawValue);
                } catch (Exception e) {
                    System.out.println("Warning: wrong iq - " + rawValue + " - skipped");
                }
            default:
                System.out.println("Warning: unknown parameter name - " + parameter.getName() + " - skipped");
                return false;
        }
    }


}
