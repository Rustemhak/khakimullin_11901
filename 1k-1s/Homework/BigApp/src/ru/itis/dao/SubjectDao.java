package ru.itis.dao;

import ru.itis.App;
import ru.itis.model.Interest;
import ru.itis.model.Subject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class SubjectDao {

    private String             filePath;
    private ArrayList<Subject> subjects;

    public SubjectDao(String filePath) {
        this.filePath = filePath;
        try {
            this.subjects = new ArrayList<Subject>();
            Scanner scanner = new Scanner(new File(filePath));
            while (scanner.hasNext()) {
                String rowString = scanner.nextLine();
                String[] s = rowString.split(";");
                Subject subject = new Subject(s[1]);
                subject.setId(Integer.parseInt(s[0]));
                subjects.add(subject);
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void save(Subject interest) {
        interest.setId((++App.lastIndex)); // сначала увеличиваем
        subjects.add(interest);
    }

    public ArrayList<Subject> findAll() {
        return subjects;
    }

    public void delete(int id) {
        subjects.remove(findById(id));
    }

    public void update(Subject interest) {
        int index = subjects.indexOf(findById(interest.getId()));
        subjects.set(index, interest);
    }

    public Subject findById(int id) {
        for (Subject interest : subjects)
            if (interest.getId() == id)
                return interest;
        return null;
    }

    public void close() {
        try {
            PrintWriter printWriter = new PrintWriter(new File(filePath));
            for (Subject interest : subjects) {
                printWriter.print(interest.getId() + ";");
                printWriter.println(interest.getName());
            }
            printWriter.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

}
