package ru.itis.dao;

import ru.itis.App;
import ru.itis.model.Interest;
import ru.itis.model.Skill;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class SkillDao {

    private String           filePath;
    private ArrayList<Skill> skills;

    public SkillDao(String filePath) {
        this.filePath = filePath;
        try {
            this.skills = new ArrayList<Skill>();
            Scanner scanner = new Scanner(new File(filePath));
            while (scanner.hasNext()) {
                String rowString = scanner.nextLine();
                String[] s = rowString.split(";");
                Skill skill = new Skill(s[1]);
                skill.setId(Integer.parseInt(s[0]));
                skills.add(skill);
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void save(Skill interest) {
        interest.setId((++App.lastIndex)); // сначала увеличиваем
        skills.add(interest);
    }

    public ArrayList<Skill> findAll() {
        return skills;
    }

    public void delete(int id) {
        skills.remove(findById(id));
    }

    public void update(Skill interest) {
        int index = skills.indexOf(findById(interest.getId()));
        skills.set(index, interest);
    }

    public Skill findById(int id) {
        for (Skill interest : skills)
            if (interest.getId() == id)
                return interest;
        return null;
    }

    public void close() {
        try {
            PrintWriter printWriter = new PrintWriter(new File(filePath));
            for (Skill interest : skills) {
                printWriter.print(interest.getId() + ";");
                printWriter.println(interest.getName());
            }
            printWriter.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

}
