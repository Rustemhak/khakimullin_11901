package ru.itis.dao;

import ru.itis.App;
import ru.itis.model.Gender;
import ru.itis.model.Student;
import ru.itis.util.Parameter;
import ru.itis.util.ParameterUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class StudentDao {

    private String             filePath;
    private ArrayList<Student> students;

    public StudentDao(String filePath) {
        this.filePath = filePath;
        try {
            this.students = new ArrayList<>();
            Scanner scanner = new Scanner(new File(filePath));
            while (scanner.hasNext()) {
                String rowString = scanner.nextLine();
                String[] s = rowString.split(";");
                Student student = new Student();
                student.setId(Integer.parseInt(s[0]));
                student.setFio(s[1]);
                student.setCity(s[2]);
                student.setBirthday(s[3]);
                student.setInstitute(s[4]);
                student.setGender(s[5].equals("null") ? null : Integer.parseInt(s[5]) == 1 ? Gender.MALE : Gender.FEMALE);
                student.setUsername(s[6]);
                student.setGroup(s[7]);
                student.setIq(Integer.parseInt(s[8]));
                students.add(student);
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void save(Student student) {
        student.setId((++App.lastIndex)); // сначала увеличиваем
        students.add(student);
    }

    public ArrayList<Student> findAll() {
        return students;
    }

    public ArrayList<Student> findByParameter(Parameter parameter) {
        ArrayList<Student> students = new ArrayList<>();
        for (Student student : this.students) {
            if (ParameterUtil.matchParameter(student, parameter)) {
                students.add(student);
            }
        }
        return students;
    }

    public void delete(int id) {
        students.remove(findById(id));
    }

    public void update(Student student) {
        int index = students.indexOf(findById(student.getId()));
        students.set(index, student);
    }

    public Student findById(int id) {
        for (Student student : students)
            if (student.getId() == id)
                return student;
        return null;
    }

    public void close() {
        try {
            PrintWriter printWriter = new PrintWriter(new File(filePath));
            for (Student student : students) {
                printWriter.print(student.getId() + ";");
                printWriter.print(student.getFio() + ";");
                printWriter.print(student.getCity() + ";");
                printWriter.print(student.getBirthday() + ";");
                printWriter.print(student.getInstitute() + ";");
                printWriter.print((student.getGender() != null? (student.getGender().equals(Gender.MALE) ? 1 : 0) : "null") + ";" );
                printWriter.print(student.getUsername() + ";");
                printWriter.print(student.getGroup() + ";");
                printWriter.println(student.getIq());
            }
            printWriter.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

}
