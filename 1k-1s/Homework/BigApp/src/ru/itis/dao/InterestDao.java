package ru.itis.dao;

import ru.itis.App;
import ru.itis.model.Interest;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class InterestDao {

    private String              filePath;
    private ArrayList<Interest> interests;

    public InterestDao(String filePath) {
        this.filePath = filePath;
        try {
            this.interests = new ArrayList<>();
            Scanner scanner = new Scanner(new File(filePath));
            while (scanner.hasNext()) {
                String rowString = scanner.nextLine();
                String[] s = rowString.split(";");
                Interest interest = new Interest(s[1]);
                interest.setId(Integer.parseInt(s[0]));
                interests.add(interest);
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void save(Interest interest) {
        interest.setId((++App.lastIndex)); // сначала увеличиваем
        interests.add(interest);
    }

    public ArrayList<Interest> findAll() {
        return interests;
    }

    public void delete(int id) {
        interests.remove(findById(id));
    }

    public void update(Interest interest) {
        int index = interests.indexOf(findById(interest.getId()));
        interests.set(index, interest);
    }

    public Interest findById(int id) {
        for (Interest interest : interests)
            if (interest.getId() == id)
                return interest;
        return null;
    }

    public void close() {
        try {
            PrintWriter printWriter = new PrintWriter(new File(filePath));
            for (Interest interest : interests) {
                printWriter.print(interest.getId() + ";");
                printWriter.println(interest.getName());
            }
            printWriter.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
