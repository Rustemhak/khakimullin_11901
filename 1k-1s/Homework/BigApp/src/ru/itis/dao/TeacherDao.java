package ru.itis.dao;

import ru.itis.App;
import ru.itis.model.Gender;
import ru.itis.model.Teacher;
import ru.itis.util.Parameter;
import ru.itis.util.ParameterUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TeacherDao {

    private String             filePath;
    private ArrayList<Teacher> teachers;

    public TeacherDao(String filePath) {
        this.filePath = filePath;
        try {
            this.teachers = new ArrayList<>();
            Scanner scanner = new Scanner(new File(filePath));
            while (scanner.hasNext()) {
                String rowString = scanner.nextLine();
                String[] s = rowString.split(";");
                Teacher teacher = new Teacher();
                teacher.setId(Integer.parseInt(s[0]));
                teacher.setFio(s[1]);
                teacher.setCity(s[2]);
                teacher.setBirthday(s[3]);
                teacher.setInstitute(s[4]);
                teacher.setGender(s[5].equals("null") ? null : Integer.parseInt(s[5]) == 1 ? Gender.MALE : Gender.FEMALE);
                teacher.setUsername(s[6]);
                teacher.setPosition(s[7]);
                teacher.setExperience(Integer.parseInt(s[8]));
                teachers.add(teacher);
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void save(Teacher teacher) {
        teacher.setId((++App.lastIndex)); // сначала увеличиваем
        teachers.add(teacher);
    }

    public ArrayList<Teacher> findAll() {
        return teachers;
    }

    public ArrayList<Teacher> findByParameter(Parameter parameter) {
        ArrayList<Teacher> teachers = new ArrayList<>();
        for (Teacher teacher : this.teachers) {
            if (ParameterUtil.matchParameter(teacher, parameter)) {
                teachers.add(teacher);
            }
        }
        return teachers;
    }

    public void delete(int id) {
        teachers.remove(findById(id));
    }

    public void update(Teacher teacher) {
        int index = teachers.indexOf(findById(teacher.getId()));
        teachers.set(index, teacher);
    }

    public Teacher findById(int id) {
        for (Teacher teacher : teachers)
            if (teacher.getId() == id)
                return teacher;
        return null;
    }

    public void close() {
        try {
            PrintWriter printWriter = new PrintWriter(new File(filePath));
            for (Teacher teacher : teachers) {
                printWriter.print(teacher.getId() + ";");
                printWriter.print(teacher.getFio() + ";");
                printWriter.print(teacher.getCity() + ";");
                printWriter.print(teacher.getBirthday() + ";");
                printWriter.print(teacher.getInstitute() + ";");
                printWriter.print((teacher.getGender() != null ? (teacher.getGender().equals(Gender.MALE) ? 1 : 0) : "null") + ";");
                printWriter.print(teacher.getUsername() + ";");
                printWriter.print(teacher.getPosition() + ";");
                printWriter.println(teacher.getExperience());
            }
            printWriter.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

}
