package ru.itis.validators;

import ru.itis.services.*;

public class CommandValidator {

    private InterestService interestService;
    private SkillService    skillService;
    private StudentService  studentService;
    private SubjectService  subjectService;
    private TeacherService  teacherService;

    private CommandSession currentSession;

    public CommandValidator(InterestService interestService,
            SkillService skillService,
            StudentService studentService,
            SubjectService subjectService,
            TeacherService teacherService) {
        this.interestService = interestService;
        this.skillService = skillService;
        this.studentService = studentService;
        this.subjectService = subjectService;
        this.teacherService = teacherService;
    }

    public String process(String command) {
        if (currentSession == null || currentSession.isProcessed()) {
            String[] input = command.split(" ", 2); // максимум 2 слова
            String heading = input[0];
            String body = input.length > 1 ? input[1] : null;
            switch (heading) {
                case "new":
                    currentSession
                            = new CommandSessionNew(interestService, skillService, studentService, subjectService, teacherService);
                    return currentSession.process(body);
                case "list":
                    currentSession
                            = new CommandSessionList(interestService, skillService, studentService, subjectService, teacherService);
                    return currentSession.process(body);
                case "exit":
                    return "closed";
                default:
                    return "unrecognizable";
            }
        } else {
            return currentSession.process(command);
        }
    }

}
