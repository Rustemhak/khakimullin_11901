package ru.itis.validators;

import ru.itis.services.*;
import ru.itis.util.Parameter;

import java.util.ArrayList;

public class CommandSessionNew extends CommandSession {

    private InterestService interestService;
    private SkillService    skillService;
    private StudentService  studentService;
    private SubjectService  subjectService;
    private TeacherService  teacherService;

    private String               type;
    private ArrayList<Parameter> parameters;

    private final String STUDENT  = "student";
    private final String TEACHER  = "teacher";
    private final String SKILL    = "skill";
    private final String SUBJECT  = "subject";
    private final String INTEREST = "interest";

    private boolean isFirst;

    private boolean hasFio;
    private boolean hasBirthday;

    public CommandSessionNew(InterestService interestService, SkillService skillService, StudentService studentService, SubjectService subjectService, TeacherService teacherService) {
        this.interestService = interestService;
        this.skillService = skillService;
        this.studentService = studentService;
        this.subjectService = subjectService;
        this.teacherService = teacherService;
        isFirst = true;
    }

    @Override
    public String process(String command) {
        StringBuilder result = new StringBuilder();
        if (isFirst) {
            isFirst = false;
            String[] input = command.split(" ", 2); // максимум 2 слова
            type = input[0];

            String[] parameterArray = input.length > 1 ? input[1].split(" ") : new String[0];
            if (type.equals(STUDENT) || type.equals(TEACHER) || type.equals(SUBJECT) || type.equals(INTEREST) || type.equals(SKILL)) {
                parameters = new ArrayList<>();
                for (String s : parameterArray) {
                    String[] rawParameter = s.split("=");
                    if (rawParameter[0].equals("fio")) hasFio = true;
                    if (rawParameter[0].equals("birthday")) hasBirthday = true;
                    parameters.add(new Parameter(rawParameter[0], rawParameter[1]));
                }
            } else if (type.equals("user")) {
                result.append("Please specify user type: student or teacher\n");
                setProcessed(true);
                return result.toString();
            } else {
                result.append("Unknown type: use student or teacher\n");
                setProcessed(true);
                return result.toString();
            }

            return tryApplyCommand();

        } else {
            if (type.equals(STUDENT) || type.equals(TEACHER)) {
                if (!hasFio) {
                    hasFio = true;
                    parameters.add(new Parameter("fio", command));
                    return tryApplyCommand();
                } else {
                    hasBirthday = true;
                    parameters.add(new Parameter("birthday", command));
                    return tryApplyCommand();
                }
            } else {
                return null; // сюда нельзя попасть
            }
        }
    }

    private String tryApplyCommand() {
        if (type.equals(STUDENT) || type.equals(TEACHER)) {
            if (hasFio && hasBirthday) {
                setProcessed(true);
                applyCommand();
                return "Added";
            } else if (!hasFio) {
                return "Fio = ";
            } else {
                return "Birthday = ";
            }
        } else {
            setProcessed(true);
            applyCommand();
            return "Added";
        }
    }

    private void applyCommand() {
        if (type.equals(TEACHER)) {
            teacherService.create(parameters);
        } else if (type.equals(STUDENT)) {
            studentService.create(parameters);
        } else if (type.equals(SKILL)) {
            skillService.create(parameters.get(0).getValue()); // достаем имя
        } else if (type.equals(SUBJECT)) {
            subjectService.create(parameters.get(0).getValue()); // достаем имя
        } else if (type.equals(INTEREST)) {
            interestService.create(parameters.get(0).getValue()); // достаем имя
        }
    }

}
