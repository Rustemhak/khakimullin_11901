package ru.itis.validators;

import ru.itis.model.*;
import ru.itis.services.*;

public class CommandSessionList extends CommandSession {

    private InterestService interestService;
    private SkillService    skillService;
    private StudentService  studentService;
    private SubjectService  subjectService;
    private TeacherService  teacherService;

    public CommandSessionList(InterestService interestService,
            SkillService skillService,
            StudentService studentService,
            SubjectService subjectService,
            TeacherService teacherService) {
        this.interestService = interestService;
        this.skillService = skillService;
        this.studentService = studentService;
        this.subjectService = subjectService;
        this.teacherService = teacherService;
    }

    @Override
    public String process(String command) {
        StringBuilder builder = new StringBuilder();
        switch (command) {
            case "student":
                for (Student student : studentService.getAll()) {
                    builder.append(student);
                }
                break;
            case "teachers":
                for (Teacher teacher : teacherService.getAll())
                    builder.append(teacher);
                break;
            case "subject":
                for (Subject subject : subjectService.getAll())
                    builder.append(subject);
                break;
            case "skill":
                for (Skill skill : skillService.getAll())
                    builder.append(skill);
                break;
            case "interest":
                for (Interest interest : interestService.getAll())
                    builder.append(interest);
                break;
            default:
                builder.append("Unknown type");
        }

        setProcessed(true);
        return builder.toString();
    }

}
