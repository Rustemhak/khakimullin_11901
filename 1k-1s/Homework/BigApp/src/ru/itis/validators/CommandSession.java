package ru.itis.validators;

public abstract class CommandSession {

    private boolean isProcessed;

    public CommandSession() {
        this.isProcessed = false;
    }

    public abstract String process(String command);

    public boolean isProcessed() {
        return this.isProcessed;
    }

    public void setProcessed(boolean processed) {
        isProcessed = processed;
    }
}
