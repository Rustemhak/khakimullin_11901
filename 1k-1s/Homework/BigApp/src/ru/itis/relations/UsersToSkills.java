package ru.itis.relations;

public class UsersToSkills {

    private int userId;
    private int skillId;

    public UsersToSkills(int userId, int skillId) {
        this.userId = userId;
        this.skillId = skillId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getSkillId() {
        return skillId;
    }

    public void setSkillId(int skillId) {
        this.skillId = skillId;
    }
}
