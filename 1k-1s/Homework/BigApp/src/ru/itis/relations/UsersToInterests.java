package ru.itis.relations;

public class UsersToInterests {

    private int userId;
    private int interestId;

    public UsersToInterests(int userId, int interestId) {
        this.userId = userId;
        this.interestId = interestId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getInterestId() {
        return interestId;
    }

    public void setInterestId(int interestId) {
        this.interestId = interestId;
    }
}
