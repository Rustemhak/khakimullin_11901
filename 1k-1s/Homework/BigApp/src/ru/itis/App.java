package ru.itis;

import ru.itis.dao.*;
import ru.itis.services.*;
import ru.itis.validators.CommandValidator;
import ru.itis.view.ConsoleView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class App {

    public static int lastIndex;

    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File("index.txt"));
        lastIndex = scanner.nextInt();
        scanner.close();

        StudentDao studentDao = new StudentDao("student_db.txt");
        TeacherDao teacherDao = new TeacherDao("student_db.txt");
        SkillDao skillDao = new SkillDao("skill_db.txt");
        SubjectDao subjectDao = new SubjectDao("subject_db.txt");
        InterestDao interestDao = new InterestDao("interest_db.txt");
        StudentService studentService = new StudentService(studentDao);
        TeacherService teacherService = new TeacherService(teacherDao);
        SkillService skillService = new SkillService(skillDao);
        SubjectService subjectService = new SubjectService(subjectDao);
        InterestService interestService = new InterestService(interestDao);
        CommandValidator commandValidator = new CommandValidator(interestService, skillService, studentService, subjectService, teacherService);
        ConsoleView consoleView = new ConsoleView(commandValidator);
        consoleView.run();

        studentDao.close();
        teacherDao.close();
        skillDao.close();
        subjectDao.close();
        interestDao.close();

        PrintWriter printWriter = new PrintWriter(new File("index.txt"));
        printWriter.println(lastIndex);
        printWriter.close();
    }

}
