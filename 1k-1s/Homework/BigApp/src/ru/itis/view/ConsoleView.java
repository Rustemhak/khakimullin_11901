package ru.itis.view;

import ru.itis.validators.CommandValidator;

import java.util.Scanner;

public class ConsoleView {

    private CommandValidator commandValidator;

    private Scanner scanner;

    public ConsoleView(CommandValidator commandValidator) {
        this.commandValidator = commandValidator;
        this.scanner = new Scanner(System.in);
    }

    public void run() {
        while (true) {
            String input = scanner.nextLine();
            String output = commandValidator.process(input);
            System.out.println(output);
            if (input.equals("exit")) // проверка на exit в конце так как надо закрыть файлы
                return;
        }
    }

}
