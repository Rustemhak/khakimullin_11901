package ru.itis.model;

import java.util.ArrayList;
import java.util.List;

public class Teacher extends User {

    private String        position; // должность
    private int           experience; // стаж
    private ArrayList<Integer> subjectIdList;

    public Teacher() {
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }


    @Override
    public String toString() {
        return "Teacher{" +
                "position='" + position + '\'' +
                ", experience=" + experience +
                ", subjectIdList=" + subjectIdList +
                ", " + super.toString() +
                "} ";
    }

    public void setSubjectIdList(ArrayList<Integer> subjectIdList) {
        this.subjectIdList = subjectIdList;
    }
}
