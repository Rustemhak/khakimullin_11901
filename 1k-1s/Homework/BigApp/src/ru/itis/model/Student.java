package ru.itis.model;

import java.util.List;

public class Student extends User {

    private String group;
    private int    iq;

    public Student() {
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    @Override
    public String toString() {
        return "Student{" +
                "group='" + group + '\'' +
                ", iq=" + iq +
                ", " + super.toString() +
                '}';
    }
}
