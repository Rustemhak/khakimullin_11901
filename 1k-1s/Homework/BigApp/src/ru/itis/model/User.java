package ru.itis.model;

import java.util.ArrayList;
import java.util.List;

public class User {

    private int           id;
    private String        fio;
    private String        city;
    private String        birthday;
    private String        institute;
    private Gender        gender;
    private String        username;
    private ArrayList<Integer> skillIdList;
    private ArrayList<Integer> interestIdList;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getInstitute() {
        return institute;
    }

    public void setInstitute(String institute) {
        this.institute = institute;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }



    @Override
    public String toString() {
        return "" +
                "id=" + id +
                ", fio='" + fio + '\'' +
                ", city='" + city + '\'' +
                ", birthday='" + birthday + '\'' +
                ", institute='" + institute + '\'' +
                ", gender=" + gender +
                ", username='" + username + '\'' +
                ", skillIdList=" + skillIdList +
                ", interestIdList=" + interestIdList;
    }

    public void setInterestIdList(ArrayList<Integer> interestIdList) {
        this.interestIdList = interestIdList;
    }

    public void setSkillIdList(ArrayList<Integer> skillIdList) {
        this.skillIdList = skillIdList;
    }
}
