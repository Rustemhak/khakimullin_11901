package ru.itis.services;

import ru.itis.dao.SubjectDao;
import ru.itis.model.Skill;
import ru.itis.model.Subject;

import java.util.ArrayList;

public class SubjectService {

    private SubjectDao subjectDao;

    public SubjectService(SubjectDao subjectDao) {
        this.subjectDao = subjectDao;
    }

    public void create(String name) {
        subjectDao.save(new Subject(name));
    }

    public ArrayList<Subject> getAll() {
        return subjectDao.findAll();
    }

    public void remove(int id) {
        subjectDao.delete(id);
    }

    public void update(Subject subject, String name) {
        subject.setName(name);
        subjectDao.update(subject);
    }
}
