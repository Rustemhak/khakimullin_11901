package ru.itis.services;

import ru.itis.dao.TeacherDao;
import ru.itis.model.Teacher;
import ru.itis.util.Parameter;
import ru.itis.util.ParameterUtil;

import java.util.ArrayList;

public class TeacherService {

    private TeacherDao teacherDao;

    public TeacherService(TeacherDao teacherDao) {
        this.teacherDao = teacherDao;
    }

    public void create(ArrayList<Parameter> parameterList) {
        Teacher teacher = new Teacher();
        for (Parameter parameter : parameterList) {
            ParameterUtil.applyParameter(teacher, parameter);
        }
        teacherDao.save(teacher);
    }

    public ArrayList<Teacher> getAll() {
        return teacherDao.findAll();
    }

    public ArrayList<Teacher> getByParameter(Parameter parameter) {
        return teacherDao.findByParameter(parameter);
    }

    public void remove(int id) {
        teacherDao.delete(id);
    }

    public void update(Teacher teacher, Parameter parameter) {
        ParameterUtil.applyParameter(teacher, parameter);
        teacherDao.update(teacher);
    }
}
