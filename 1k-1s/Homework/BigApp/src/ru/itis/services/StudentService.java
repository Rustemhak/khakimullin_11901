package ru.itis.services;

import ru.itis.dao.StudentDao;
import ru.itis.model.Gender;
import ru.itis.model.Student;
import ru.itis.util.Parameter;
import ru.itis.util.ParameterUtil;

import java.util.ArrayList;

public class StudentService {

    private StudentDao studentDao;

    public StudentService(StudentDao studentDao) {
        this.studentDao = studentDao;
    }

    public void create(ArrayList<Parameter> parameterList) {
        Student student = new Student();
        for (Parameter parameter : parameterList) {
            ParameterUtil.applyParameter(student, parameter);
        }
        studentDao.save(student);
    }

    public ArrayList<Student> getAll() {
        return studentDao.findAll();
    }

    public ArrayList<Student> getByParameter(Parameter parameter) {
        return studentDao.findByParameter(parameter);
    }

    public void remove(int id) {
        studentDao.delete(id);
    }

    public void update(Student student, Parameter parameter) {
        ParameterUtil.applyParameter(student, parameter);
        studentDao.update(student);
    }

}
