package ru.itis.services;

import ru.itis.dao.InterestDao;
import ru.itis.model.Interest;

import java.util.ArrayList;

public class InterestService {

    private InterestDao interestDao;

    public InterestService(InterestDao interestDao) {
        this.interestDao = interestDao;
    }

    public void create(String name) {
        interestDao.save(new Interest(name));
    }

    public ArrayList<Interest> getAll() {
        return interestDao.findAll();
    }

    public void remove(int id) {
        interestDao.delete(id);
    }

    public void update(Interest interest, String name) {
        interest.setName(name);
        interestDao.update(interest);
    }


}
