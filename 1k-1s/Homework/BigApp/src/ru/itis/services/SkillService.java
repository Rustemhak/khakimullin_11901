package ru.itis.services;

import ru.itis.dao.SkillDao;
import ru.itis.model.Skill;

import java.util.ArrayList;

public class SkillService {

    private SkillDao skillDao;

    public SkillService(SkillDao skillDao) {
        this.skillDao = skillDao;
    }

    public void create(String name) {
        skillDao.save(new Skill(name));
    }

    public ArrayList<Skill> getAll() {
        return skillDao.findAll();
    }

    public void remove(int id) {
        skillDao.delete(id);
    }

    public void update(Skill skill, String name) {
        skill.setName(name);
        skillDao.update(skill);
    }

}
