/**
 * @author Rustem Khakimullin
 * 11-901
 * Task 45
 */
public class RationalComplexNumber {
    private RationalFraction re, im;

    public RationalComplexNumber() {
        RationalFraction rf = new RationalFraction();
        this.re = rf;
        this.im = rf;
    }

    public RationalComplexNumber(RationalFraction re, RationalFraction im) {
        this.re = re;
        this.im = im;
    }

    public RationalFraction getRe() {
        return re;
    }

    public RationalFraction getIm() {
        return im;
    }

    public RationalComplexNumber add(RationalComplexNumber rc) {
        return new RationalComplexNumber(this.re.add(rc.getRe()), this.im.add(rc.getIm()));
    }

    public RationalComplexNumber sub(RationalComplexNumber rc) {
        return new RationalComplexNumber(this.re.sub(rc.getRe()), this.im.sub(rc.getIm()));
    }

    public RationalComplexNumber mult(RationalComplexNumber rc) {
        return new RationalComplexNumber(this.re.mult(rc.getRe()).sub(this.im.mult(rc.getIm())),
                this.re.mult(rc.getIm()).add(this.im.mult(rc.getRe())));
    }

    @Override
    public String toString() {
        if (this.re.value() < 0.0)
            return this.im.toString() + " * i " + this.re.toString();
        else return this.im.toString() + " * i + " + this.re.toString();
    }
}
