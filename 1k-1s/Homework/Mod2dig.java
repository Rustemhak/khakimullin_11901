/**
* @author Rustem Khakimullin
* 11-901
* Digit mod 2
*/

public class Mod2dig {
	public static void main(String [] args) {
		int a = Integer.parseInt(args[0]);
		int k = 0;
		while(a != 0){
			if((a % 10) % 2 == 0)
				k++;
			a /= 10;
		}
		System.out.println(k > 0 ? "Yes" : "No");
	}
}