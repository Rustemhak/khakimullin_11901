/**
 * @author Rustem Khakimullin
 * 11-901
 * Task 43
 */
public class RationalMatrix2x2 {
    private RationalFraction[][] a = new RationalFraction[2][2];

    public RationalMatrix2x2() {
        RationalFraction rf = new RationalFraction();
        this.a[0][0] = rf;
        this.a[0][1] = rf;
        this.a[1][0] = rf;
        this.a[1][1] = rf;
    }

    public RationalMatrix2x2(RationalFraction a) {
        this(a, a, a, a);
    }

    public RationalMatrix2x2(RationalFraction a00, RationalFraction a01, RationalFraction a10, RationalFraction a11) {
        this.a[0][0] = a00;
        this.a[0][1] = a01;
        this.a[1][0] = a10;
        this.a[1][1] = a11;
    }

    public RationalFraction[][] getA() {
        return a;
    }

    public RationalMatrix2x2(RationalFraction[][] a) {
        this(a[0][0], a[0][1], a[1][0], a[1][1]);
    }

    public RationalMatrix2x2 add(RationalMatrix2x2 rm) {
        RationalFraction[][] a = new RationalFraction[2][2];
        for (int i = 0; i < 2; i++)
            for (int j = 0; j < 2; j++)
                a[i][j] = this.a[i][j].add(rm.getA()[i][j]);
        return new RationalMatrix2x2(a);
    }

    public RationalMatrix2x2 mult(RationalMatrix2x2 rm) {
        RationalFraction[][] a = new RationalFraction[2][2];
        for (int i = 0; i < 2; i++)
            for (int j = 0; j < 2; j++)
                for (int k = 0; k < 2; k++)
                    a[i][j] = a[i][j].add(this.a[i][k].mult(rm.getA()[k][j]));
        return new RationalMatrix2x2(a);
    }

    public RationalFraction det() {
        return this.a[0][0].mult(this.a[1][1]).sub(this.a[1][0].mult(this.a[0][1]));
    }

    public RationalVector2D multVector(RationalVector2D rv) {
        return new RationalVector2D(this.a[0][0].mult(rv.getX()).add(this.a[0][1].mult(rv.getY())),
                this.a[1][0].mult(rv.getX()).add(this.a[1][1].mult(rv.getY())));
    }
}
