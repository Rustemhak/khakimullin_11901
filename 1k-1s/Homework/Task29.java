/**
* @author Rustem Khakimullin
* 11-901
* Task 29
*/
import java.util.Random;
import java.util.regex.*;
public class Task29 {
	public static void main(String [] args){
		int cnt = 0;
		Random r = new Random();
		Pattern p = Pattern.compile("[02468]{4}|[02468]{5}");
		int  n = 0;
		while (cnt < 10){
			int x = r.nextInt(1000000);
			n++;
			String s = Integer.toString(x);
			Matcher m  =  p.matcher(s);
			if (m.matches()){
				System.out.println(s);
				cnt++;
			}
		}
		System.out.println("Общее количество сгенерированных чисел: " + n);
	}
}