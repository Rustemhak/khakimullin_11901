/**
 * @author Rustem Khakimullin
 * 11-901
 * Task 41
 */

public class RationalVector2D {
    private RationalFraction x, y;

    public static void main(String[] args) {
        RationalFraction rf1 = new RationalFraction(1, 2);
        RationalFraction rf2 = new RationalFraction(1, 3);
        RationalVector2D rv = new RationalVector2D(rf1, rf2);
        System.out.println(rv);
    }

    public RationalVector2D(RationalFraction x, RationalFraction y) {
        this.x = x;
        this.y = y;
    }

    public RationalFraction getX() {
        return x;
    }

    public RationalFraction getY() {
        return y;
    }

    public RationalVector2D() {
        RationalFraction rf = new RationalFraction();
        this.x = rf;
        this.y = rf;
    }

    public RationalVector2D add(RationalVector2D rv) {
        return new RationalVector2D(this.x.add(rv.getX()),
                this.y.add(rv.getY()));
    }

    public double length() {
        return Math.sqrt((this.x.mult(this.x).add(this.y.mult(this.y))).value());
    }

    public RationalFraction scalarProduct(RationalVector2D rv) {
        return (this.x.mult(rv.getX()).add(this.y.mult(rv.getY())));

    }

    public boolean equals(RationalVector2D rv) {
        if (this.x.equals(rv.getX()) && this.y.equals(rv.getY()))
            return true;
        else return false;
    }

    @Override
    public String toString() {
        return "(" + this.x.toString() + ";" + this.y.toString() + ")";
    }
}
