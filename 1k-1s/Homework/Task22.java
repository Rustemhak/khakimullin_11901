/**
* @author Rustem Khakimullin
* 11-901
* Task 22
*/
import java.util.Scanner;
public class Task22{
	public static void count(String s, int alph[]){
		 String [] word = s.split(" ");
		 for (String i: word){
			 for(int j = 0; j < i.length(); j++){
				 if (i.charAt(j) >= 'A' && i.charAt(j) <= 'Z')
					alph[i.charAt(j) - 'A']++;
				else if (i.charAt(j) >= 'a' && i.charAt(j) <= 'z')
					alph[i.charAt(j) - 'a']++;
			 }
		 }
	}
	public static void main(String [] args){
		 Scanner in = new Scanner(System.in);
		 String s = in.nextLine();
		 int[] alph = new int [26];
		 count(s,alph);
		 for (int i = 0; i < 26; i++)
			 System.out.println((char)('A' + i) + ": " + alph[i]);
	}
}