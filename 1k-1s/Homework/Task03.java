/**
* @author Rustem Khakimullin
* 11-901
* Task 03
*/

public class Task03 {
	public static void main(String [] args) {
		int a = Integer.parseInt(args[0]);
		int s = 0;
		while (a != 0) {
			s += a % 10;
			a /= 10;
		}
		System.out.println(s);
	}
}