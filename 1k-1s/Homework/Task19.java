/**
* @author Rustem Khakimullin
* 11-901
* Task 19
*/
 public class Task19{
	 public static String compare(String s1, String s2){
		 int min = s1.length();
		 if (s2.length() < min)
			 min = s2.length();
		 for (int i = 0; i < min; i++){
			if (s1.charAt(i) > s2.charAt(i))
				return "Первая строка позже";
			else if (s1.charAt(i) < s2.charAt(i))
				return "Первая строка раньше";
		}
		if (s1.length() > min)
			return "Первая строка позже";
		else if (s2.length() > min)
			return "Первая строка раньше";
		else return "Они равны";
	 }
	 public static void main(String [] args){
		 String s1 = args[0], s2 = args[1];
		 System.out.println(compare(s1,s2));
	 }
 }