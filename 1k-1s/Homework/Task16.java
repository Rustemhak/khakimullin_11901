/**
* @author Rustem Khakimullin
* 11-901
* Task16
*/

public class Task16{
	static final double EPS = 1e-9;
	
	 static double ln(double x){ //ln(1 + x)
		int i = 1;
		int sign = 1;
		double deg = x;
		double a = x ;
		double s = a;
		while (Math.abs(a) > EPS){
			i++;
			sign *= (-1);
			deg *= x;
			a = sign * deg / i;
			s += a;
		}
		return s;
	}
	
	public static void main(String []  args){
		double x = Double.parseDouble(args[0]);
		double ans = 0;
		if (x < 1)
			ans = ln(--x);//ln(1+x)
		else if (x >= 1){
			/*
			Insofar as we know how count ln(1+a) where 1 > a > -1 by Maclaurin series
			And we know x, then can reduce to ln(x) = ln(1 + t) - ln(1 + (-t)).
			Where 1 > t > -1 <=> 1 > -t > -1.
			And we  can count ln(1 + (-t)) and ln(1 + t) by Maclaurin series.
			By logarithm property we have ln(x)=ln((1 + t)/((1 - t)) => x = (1 + t)/(1 - t), x >= 1 - if;
			Solve equality and get t = (x - 1) / (x + 1);
			*/
			double t = (x - 1) / (x + 1);
			ans = ln(t) - ln(-t);
		}
		System.out.println(ans);
	}
}