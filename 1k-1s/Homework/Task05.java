/**
* @author Rustem Khakimullin
* 11-901
* Task 05
*/

public class Task05 {
	public static void main(String [] args) {
		int n = Integer.parseInt(args[0]);
		for (int i = 0; i < n; i++){
			for(int j = 0;j < n-i; j++)
					System.out.print(" ");
				for(int j = 0; j < n; j++)
					System.out.print(1);
				 System.out.println();
			
		}
	}
}
