/**
* @author Rustem Khakimullin
* 11-901
* Task 09
*/
public class Task09 {
	public static void main(String [] args) {
		double x = Double.parseDouble(args[0]);
		double y = Double.parseDouble(args[1]);
		boolean flag = false;
		if(y >= 0.0 && y + Math.abs(x) <= 1.0)
			flag = true;
		System.out.println(flag ? "Yes" : "No");
	}
}