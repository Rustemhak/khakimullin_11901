/**
 * @author Rustem Khakimullin
 * 11-901
 * Task 44
 */

public class ComplexMatrix2x2 {
    private ComplexNumber[][] a = new ComplexNumber[2][2];

    public ComplexMatrix2x2() {
        ComplexNumber cn = new ComplexNumber();
        this.a[0][0] = cn;
        this.a[0][1] = cn;
        this.a[1][0] = cn;
        this.a[1][1] = cn;
    }

    public ComplexMatrix2x2(ComplexNumber a) {
        this(a, a, a, a);
    }

    public ComplexNumber[][] getA() {
        return a;
    }

    public ComplexMatrix2x2(ComplexNumber a00, ComplexNumber a01, ComplexNumber a10, ComplexNumber a11) {
        this.a[0][0] = a00;
        this.a[0][1] = a01;
        this.a[1][0] = a10;
        this.a[1][1] = a11;
    }

    public ComplexMatrix2x2(ComplexNumber a[][]) {
        this(a[0][0], a[0][1], a[1][0], a[1][1]);
    }

    public ComplexMatrix2x2 add(ComplexMatrix2x2 cm) {
        ComplexNumber[][] a = new ComplexNumber[2][2];
        for (int i = 0; i < 2; i++)
            for (int j = 0; j < 2; j++)
                a[i][j] = this.a[i][j].add(cm.getA()[i][j]);
        return new ComplexMatrix2x2(a);
    }

    public ComplexMatrix2x2 mult(ComplexMatrix2x2 cm) {
        ComplexNumber[][] a = new ComplexNumber[2][2];
        for (int i = 0; i < 2; i++)
            for (int j = 0; j < 2; j++)
                for (int k = 0; k < 2; k++)
                    a[i][j] = a[i][j].add(this.a[i][k].mult(cm.getA()[k][j]));
        return new ComplexMatrix2x2(a);
    }

    public ComplexNumber det() {
        return this.a[0][0].mult(this.a[1][1]).sub(this.a[1][0].mult(this.a[0][1]));
    }

    public ComplexVector2D multVector(ComplexVector2D cv) {
        return new ComplexVector2D(this.a[0][0].mult(cv.getX()).add(this.a[0][1].mult(cv.getY())),
                this.a[1][0].mult(cv.getX()).add(this.a[1][1].mult(cv.getY())));
    }

}
