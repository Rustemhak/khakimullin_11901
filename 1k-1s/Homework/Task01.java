/**
* @author Rustem Khakimullin
* 11-901
* Task 01
*/
public class Task01 {
	public static void main( String [] args) {
		long x = Long.parseLong(args[0]);
		x *= x; // 2
		long x2 = x;
		x *= x; // 4
		x *= x; // 8
		x *= x; // 16
		long x16 = x;
		x *= x; // 32
		x *= (x2 * x16);
		System.out.println(x);
	}
}