/**
* @author Rustem Khakimullin
* 11-901
* Task17
*/


public class Task17{
	public static void main(String [] args){
		double x = Double.parseDouble(args[0]);
		final double EPS=1e-9;
		double deg = x;
		int k = 1;
		int sign = 1;
		k++;
		double a = x;
		double sum = a;
		while(Math.abs(a) > EPS){
			sign *= - 1;
			deg *= x;
			a = sign * deg / k;
			sum += a;
			k++;
			//System.out.println(sum);
		}
		System.out.println(sum);
	}
}