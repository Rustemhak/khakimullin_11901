/**
* @author Rustem Khakimullin
* 11-901
* Task 07 with methods
*/

public class Task07 {
	
	public static void newLine(){
		System.out.println();
	}
	 
	public static void drawLine(String s, int k){
		for (int j = 0; j < k; j++)
			System.out.print(s);
	}
	
	public static void triangle(int k_space, int i){
		drawLine(" ", k_space);
		drawLine("1", i);
	}
	
	public static void main(String args []) {
		int height = Integer.parseInt(args[0]);
		int i = 1;
		int quantity = 1;
		for(int j = 0; j < height-1; j++)
			quantity += 2;
		quantity++;
		
		//top
		int kspace = (quantity - i/2);
		while(i <= quantity){
			triangle(kspace, i);
			i += 2;
			newLine();
			kspace--;
		}
		
		newLine();
		//bot
		i = 1;
		while(i <= quantity){
			triangle(kspace, i);
			triangle(quantity - i, i);
			i += 2;
			newLine();
			kspace--;
		}
	}
}
