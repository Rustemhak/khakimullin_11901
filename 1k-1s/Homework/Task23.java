/**
* @author Rustem Khakimullin
* 11-901
* Task 23
*/
import java.util.Scanner;
 public class Task23{
	 public static boolean exists(String s, String sub, int i){
		 for (int j = 0; j < sub.length(); j++)
			 if (sub.charAt(j) != s.charAt(i))
				 return false;
			 else i++;
		return true;
	}
	public static String replace2x(String s, String sub, String ins){
		int i = 0;
		int k = 0;
		String s1;
		 while(i < s.length() - sub.length() + 1){
			if (exists(s, sub, i)){
				k++;
				if (k == 2){
					k = 0;
					s1 = "";
					for (int j = 0; j < i; j++)
						s1 += s.charAt(j);
					s1 += ins;
					for (int j = i + sub.length(); j < s.length(); j++)
						s1 += s.charAt(j);
					s = s1;
				}
			}
			i++;
		 }
		 return s;
	}
	 public static void main(String [] args){
		 Scanner in = new Scanner(System.in);
		 String s = in.nextLine();
		 String sub = "true";
		 String ins = "false";
		 s = replace2x(s, sub, ins);
		 System.out.println(s);
	 }
 }