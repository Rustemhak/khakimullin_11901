/**
 * @author Rustem Khakimullin
 * 11-901
 * Task 42
 */
public class ComplexVector2D {
    private ComplexNumber x;
    private ComplexNumber y;


    public ComplexVector2D() {
        ComplexNumber cn = new ComplexNumber();
        this.x = cn;
        this.y = cn;
    }

    public ComplexVector2D(ComplexNumber x, ComplexNumber y) {
        this.x = x;
        this.y = y;
    }

    public ComplexNumber getX() {
        return x;
    }

    public ComplexNumber getY() {
        return y;
    }

    public ComplexVector2D add(ComplexVector2D cv) {
        return new ComplexVector2D(this.x.add(cv.getX()), this.y.add(cv.getY()));
    }

    @Override
    public String toString() {
        return "(" + this.x.toString() + "; " + this.y.toString() + ")";
    }

    public ComplexNumber scalarProduct(ComplexVector2D cv) {
        return (this.x.mult(cv.getX()).add(this.y.mult(cv.getY())));
    }
    public boolean equals(ComplexVector2D cv) {
        if (this.x.equals(cv.getX()) && this.y.equals(cv.getY()))
            return true;
        else return false;
    }
}
