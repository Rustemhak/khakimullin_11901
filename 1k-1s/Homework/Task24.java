/**
* @author Rustem Khakimullin
* 11-901
* Task 24
*/
import java.util.Scanner;
 public class Task24{
	 public static boolean exists(String s, String sub, int i){
		 for (int j = 0; j < sub.length(); j++)
			 if (sub.charAt(j) != s.charAt(i))
				 return false;
			 else i++;
		return true;
	}
		public static String replace(String s, String sub, String ins){
		int i = 0;
		String s1;
		 while(i < s.length() - sub.length() + 1){
			if (exists(s, sub, i)){
					s1 = "";
					for (int j = 0; j < i; j++)
						s1 += s.charAt(j);
					s1 += ins;
					for (int j = i + sub.length(); j < s.length(); j++)
						s1 += s.charAt(j);
					s = s1;
			}
			i++;
		 }
		 return s;
	}
	 public static void main(String [] args){
		 Scanner in = new Scanner(System.in);
		 String s = in.next();
		 String sub = "mum";
		 String ins = "dad";
		 
		 s = replace(s, sub, ins);
		 System.out.println(s);
	 }
 }