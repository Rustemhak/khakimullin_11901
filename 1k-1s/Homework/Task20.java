/**
* @author Rustem Khakimullin
* 11-901
* Task 20
*/
 public class Task20{
	
	  public static boolean compare(String s1, String s2){
		  int min = s1.length();
		 if (s2.length() < min)
			 min = s2.length();
		 for (int i = 0; i < min; i++){
			if (s1.charAt(i) > s2.charAt(i))
				return true;
			else if (s1.charAt(i) < s2.charAt(i))
				return false;
		}
		if (s1.length() > min)
			return true;
		else if (s2.length() > min)
			return false;
		else return false;
	 }
	 public static void sort(String[] s){
		 String swap;
		  for (int i = 0; i < s.length - 1; i++)
			 for (int j =0; j < s.length - i - 1; j++)
				 if(compare(s[j],s[j+1])){
					swap = s[j];
					s[j] = s[j+1];
					s[j+1] = swap;
				 }
	
	 }
	 public static void main(String [] args){
		 int n = Integer.parseInt(args[0]);
		 String[] s = new String[n];
		 for (int i = 0; i < n; i++)
			 s[i] = args[i+1].toLowerCase();
		 sort(s);
		 for( String i: s)
			System.out.println(i);
	 }
 }