/**
* @author Rustem Khakimullin
* 11-901
* Task 00
*/
public class Task00 {
	public static void main( String [] args) {
		System.out.println("111111   11   11   111111   11111111   11111111   111    111 ");
		System.out.println("11  11   11   11   11          11      11         1111  1111 ");
		System.out.println("111111   11   11   111111      11      11111111   11 1111 11 ");
		System.out.println("11 11    11   11       11      11      11         11  11  11 ");
		System.out.println("11  11   1111111   111111      11      11111111   11      11 ");
	}
}
