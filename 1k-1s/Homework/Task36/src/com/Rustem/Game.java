/**
 * @author Rustem Khakimullin
 * 11-901
 * Task 36
 */

package com.Rustem;

import java.util.Scanner;

public class Game {
    public void start() {
        Player p1 = new Player("Rustem");
        Player p2 = new Player("Damir");
        Scanner in = new Scanner(System.in);
        while (p1.getHp() > 0 && p2.getHp() > 0 ){
            p1.hit(p2,in.nextInt());
            if (p1.getHp() > 0 && p2.getHp() > 0 )
                p2.hit(p1,in.nextInt());
        }
        if (p1.getHp() > 0)
            System.out.println(p2.getName());
        else System.out.println(p1.getName());

    }

    public static void main(String[] args) {
        (new Game()).start();
    }
}