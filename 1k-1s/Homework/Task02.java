/**
* @author Rustem Khakimullin
* 11-901
* Task 02
*/
import java.util.Scanner;

public class Task02 {
	public static void main(String [] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int a;
		int s = 0;
		for (int i = 0; i < n; i++) {
			a = in.nextInt();
			s += a;
		}
		System.out.println(s);
	}
}