/**
 * @author Rustem Khakimullin
 * 11-901
 * Task 47
 */
public class RationalComplexMatrix2x2 {
    private RationalComplexNumber[][] a = new RationalComplexNumber[2][2];

    public RationalComplexMatrix2x2() {
        RationalComplexNumber rcn = new RationalComplexNumber();
        this.a[0][0] = rcn;
        this.a[0][1] = rcn;
        this.a[1][0] = rcn;
        this.a[1][1] = rcn;
    }

    public RationalComplexMatrix2x2(RationalComplexNumber a[][]) {
        this(a[0][0], a[0][1], a[1][0], a[1][1]);
    }

    public RationalComplexNumber[][] getA() {
        return a;
    }

    public RationalComplexMatrix2x2(RationalComplexNumber a00, RationalComplexNumber a01, RationalComplexNumber a10, RationalComplexNumber a11) {
        this.a[0][0] = a00;
        this.a[0][1] = a01;
        this.a[1][0] = a10;
        this.a[1][1] = a11;
    }

    public RationalComplexMatrix2x2 add(RationalComplexMatrix2x2 rcm) {
        RationalComplexNumber[][] a = new RationalComplexNumber[2][2];
        for (int i = 0; i < 2; i++)
            for (int j = 0; j < 2; j++)
                a[i][j] = this.a[i][j].add(rcm.getA()[i][j]);
        return new RationalComplexMatrix2x2(a);
    }

    public RationalComplexMatrix2x2 mult(RationalComplexMatrix2x2 rcm) {
        RationalComplexNumber[][] a = new RationalComplexNumber[2][2];
        for (int i = 0; i < 2; i++)
            for (int j = 0; j < 2; j++)
                for (int k = 0; k < 2; k++)
                    a[i][j] = a[i][j].add(this.a[i][k].mult(rcm.getA()[k][j]));
        return new RationalComplexMatrix2x2(a);
    }

    public RationalComplexNumber det() {
        return this.a[0][0].mult(this.a[1][1]).sub(this.a[1][0].mult(this.a[0][1]));
    }

    public RationalComplexVector2D multVector(RationalComplexVector2D rcv) {
        return new RationalComplexVector2D(this.a[0][0].mult(rcv.getX()).add(this.a[0][1].mult(rcv.getY())),
                this.a[1][0].mult(rcv.getX()).add(this.a[1][1].mult(rcv.getY())));
    }
}
