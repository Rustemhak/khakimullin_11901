/**
 * @author Rustem Khakimullin
 * 11-901
 * for Task 47
 */
public class ComplexNumber {
    private double re, im;

    public ComplexNumber() {
        this(0, 0);
    }

    public ComplexNumber(double re, double im) {
        this.re = re;
        this.im = im;
    }

    public double getRe() {
        return re;
    }

    public double getIm() {
        return im;
    }

    public void setIm(double im) {
        this.im = im;
    }

    public ComplexNumber add(ComplexNumber cn) {
        return new ComplexNumber(this.re + cn.getRe(), this.im + cn.getIm());
    }

    public void add2(ComplexNumber cn) {
        this.re += cn.getRe();
        this.im += cn.getIm();
    }

    public ComplexNumber sub(ComplexNumber cn) {
        return new ComplexNumber(this.re - cn.getRe(), this.im - cn.getIm());
    }

    public void sub2(ComplexNumber cn) {
        this.re -= cn.getRe();
        this.im -= cn.getIm();
    }

    public ComplexNumber multNumber(double k) {
        return new ComplexNumber(this.re * k, this.im * k);
    }

    public void multNumber2(double k) {
        this.re *= k;
        this.im *= k;
    }

    public ComplexNumber mult(ComplexNumber cn) {
        return new ComplexNumber(this.re * cn.getRe() - this.im * cn.getIm(), this.re * cn.getIm() + this.im * cn.getRe());
    }

    public void mult2(ComplexNumber cn) {
        this.re = this.re * cn.getRe() - this.im * cn.getIm();
        this.im = this.re * cn.getIm() + this.im * cn.getRe();
    }

    public ComplexNumber div(ComplexNumber cn) {
        double k = cn.getRe() * cn.getRe() + cn.getIm() * cn.getIm();
        k = 1 / k;
        cn.setIm((-1) * cn.getIm());
        ComplexNumber cn1 = this.mult(cn);
        return cn1.multNumber(k);
    }

    public void div2(ComplexNumber cn) {
        double k = cn.getRe() * cn.getRe() + cn.getIm() * cn.getIm();
        k = 1 / k;
        cn.setIm((-1) * cn.getIm());
        this.mult2(cn);
        this.multNumber2(k);
    }

    public double length() {
        return Math.sqrt(this.re * this.re + this.im * this.im);
    }

    @Override
    public String toString() {
        if (this.re < 0)
            return this.im + " * i " + this.re;
        else return this.im + " * i + " + this.re;
    }

    public double arg() {
        return Math.atan(this.im / this.re);
    }

    public ComplexNumber pow(double n) {
        return new ComplexNumber(Math.pow(this.length(), n) * Math.cos(n * this.arg()),
                Math.pow(this.length(),n) * Math.sin(n * this.arg()));
    }
    public  boolean equals(ComplexNumber cn){
        if (this.re == cn.getRe() && this.im == cn.getIm())
            return true;
        else return false;
    }
}
