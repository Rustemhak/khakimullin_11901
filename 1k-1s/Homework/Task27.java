/**
* @author Rustem Khakimullin
* 11-901
* Task 27
*/
import java.util.regex.*;
import java.util.Scanner;
 public class Task27{
	 public static void main(String [] args){
		 Pattern p = Pattern.compile("1+|0+|(10)+1?|(01)+0?");
		int n =Integer.parseInt(args[0]);
		String[] code = new String[n];
		for (int i = 0; i < n; i++)
			code[i] = args[i + 1];
		for (int i = 0; i < n; i++){
		Matcher m = p.matcher(code[i]);
		if(m.matches())
			System.out.println(i + 1);
		}
	 }
 }