/**
* @author Rustem Khakimullin
* 11-901
* Task 21
*/
import java.util.Scanner;
 public class Task21{
	 public static int UpCase(String s){
		 String[] word = s.split(" ");
		 int cnt = 0;
		 for (String i: word)
			 if (i.charAt(0) >= 'A' && i.charAt(0) <= 'Z')
				 cnt++;
		return cnt;
	 }
	 public static void main(String [] args){
		 Scanner in = new Scanner(System.in);
		 String s = in.nextLine();
		 System.out.println(UpCase(s));
	 }
 }
		 