/**
* @author Rustem Khakimullin
* 11-901
* Task 14 first solution
*/

public class Task14A{
	public static vo id main(String [] args) {
		double x = Double.parseDouble(args[0]);
		double y;
		if (x > 2)
			y = (x * x - 1) / (x + 2);
		else if (x > 0 && x <= 2)
			y = (x * x - 1) * (x + 2);
		else y = x * x * (1 + 2 * x);
		System.out.println(y);
	}
}
