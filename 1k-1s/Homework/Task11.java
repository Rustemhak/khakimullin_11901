/**
* @author Rustem Khakimullin
* 11-901
* Task 11
*/
public class Task11 {
	public static void main(String [] args) {
		double x = Double.parseDouble(args[0]);
		double y = Double.parseDouble(args[1]);
		boolean flag = false;
		if (x >= 0 && x * x + (y + 2) * (y + 2) >= 4 && x * x + y * y <= 16) 
			flag = true;
		else if(x * x + (y - 2) * (y - 2) <= 4)
			flag = true;
		System.out.println(flag ? "Yes" : "No");
	}
}