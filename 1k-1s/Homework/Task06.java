/**
* @author Rustem Khakimullin
* 11-901
* Task 06
*/

public class Task06 {
	public static void main(String args []) {
		int height = Integer.parseInt(args[0]);
		int i = 1;
		int quantity = 1;
		for(int j = 0; j < height-1; j++)
			quantity += 2;
		quantity++;
		while(i <= quantity){
			for(int j = 0; j < (quantity - i) / 2; j++)
				System.out.print(" ");
			for(int j = 0; j < i; j++)
				System.out.print(1);
			for(int j = 0; j < (quantity-i) / 2; j++)
				System.out.print(" ");
			i += 2;
			System.out.println();
		}
	}
}